﻿
using Administrador_Biblioteca.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Administrador_Biblioteca
{
    public partial class frmAdministrarLibro : Form
    {
        public Libro libroSeleccionadoPrestamo { get; set; }
        private Modelo.Libro libroActual = new Modelo.Libro();
        private GestorLibro gestorLibro;
        private Modelo.Configuracion CONFIG;

        public frmAdministrarLibro(Modelo.Configuracion CONFIG, GestorLibro gestorLibro)
        {
            InitializeComponent();
            this.CONFIG = CONFIG;
            this.gestorLibro = gestorLibro;
        }

        private void frmConsultarLibro_Load(object sender, EventArgs e)
        {
            cargarCombosCategoria();
            cargarCombosEditorial();
            cargarCombosAutor();

            tblAutores.Rows.Clear();
            tblAutores.Refresh();

            tblCategorias.Rows.Clear();
            tblCategorias.Refresh();

            btnActualizarLibro.Visible = false;

            if (!CONFIG.permisos.Contains("SUPER"))
            {
                checkActivoN.Enabled = false;
                btnAgregarAutorN.Enabled = false;
                btnAgregarCategoriaN.Enabled = false;
                btnActualizarLibro.Enabled = false;
                btnGuardarDatosLibro.Enabled = false;
            }
        }

        private void cargarCombosCategoria()
        {
            try
            {
                DataTable dttConsulta = gestorLibro.cargarCategorias();
                DataTable tablaDatos = dttConsulta.Clone();
                DataRow fRow = tablaDatos.NewRow();
                fRow["cod_categoria"] = "-1";
                fRow["nombre_categoria"] = "TODAS";
                tablaDatos.Rows.Add(fRow);
                foreach (DataRow r in dttConsulta.Rows)
                {
                    DataRow row = tablaDatos.NewRow();

                    row["cod_categoria"] = r["cod_categoria"];
                    row["nombre_categoria"] = r["nombre_categoria"];
                    tablaDatos.Rows.Add(row);
                }

                this.cbCategoriasB.DataSource = tablaDatos.DefaultView;
                this.cbCategoriasB.DisplayMember = "nombre_categoria";

                this.cbCategoriasN.DataSource = dttConsulta.DefaultView;
                this.cbCategoriasN.DisplayMember = "nombre_categoria";
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e.Message);
            }
        }

        private void cargarCombosEditorial()
        {
            try
            {
                DataTable dttConsulta = gestorLibro.cargarEditoriales();
                DataTable tablaDatos = dttConsulta.Clone();
                DataRow fRow = tablaDatos.NewRow();
                fRow["cod_editorial"] = "-1";
                fRow["nombre_editorial"] = "TODAS";
                tablaDatos.Rows.Add(fRow);
                foreach (DataRow r in dttConsulta.Rows)
                {
                    DataRow row = tablaDatos.NewRow();

                    row["cod_editorial"] = r["cod_editorial"];
                    row["nombre_editorial"] = r["nombre_editorial"];
                    tablaDatos.Rows.Add(row);
                }

                this.cbEditorialesB.DataSource = tablaDatos.DefaultView;
                this.cbEditorialesB.DisplayMember = "nombre_editorial";

                this.cbEditorialesN.DataSource = dttConsulta.DefaultView;
                this.cbEditorialesN.DisplayMember = "nombre_editorial";

                cbEditorialesN.Text = "";
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e.Message);
            }
        }

        private void cargarCombosAutor()
        {
            try
            {
                DataTable dttConsulta = gestorLibro.cargarAutores("");
                this.cbAutoresN.DataSource = dttConsulta.DefaultView;
                this.cbAutoresN.DisplayMember = "nombre_autor_completo";
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e.Message);
            }
        }

        private void btnAdministrarLibros_Click(object sender, EventArgs e)
        {
            frmAgregarNuevoAutor frm = new frmAgregarNuevoAutor(CONFIG, gestorLibro, "");
            frm.Show();
        }

        private void btnAgregarAutorN_Click(object sender, EventArgs e)
        {
            agregarNuevoAutorEnTabla();
        }

        private void tblAutores_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == tblAutores.Columns["control"].Index)
            {
                libroActual.autores.Remove(tblAutores.Rows[e.RowIndex].Cells["nombre_autor"].Value.ToString());
                recargarTblAutores(libroActual.autores);
            }
        }

        private void agregarNuevoAutorEnTabla()
        {
            string datoNuevo = cbAutoresN.Text;
            if (libroActual.autores == null)
            {
                libroActual.autores = new List<string>();
            }
            if (!libroActual.autores.Contains(datoNuevo))
            {
                //Si existe en el combo se agrega
                if (cbAutoresN.FindStringExact(datoNuevo) != -1)
                {
                    libroActual.autores.Add(datoNuevo);
                }
                //Si no existe en el combo se puede agregar uno nuevo si es Administrador
                else
                {
                    if (CONFIG.permisos.Contains("SUPER"))
                    {
                        DialogResult dr = MessageBox.Show("Desea agregar Autor?", "Nuevo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (dr == DialogResult.Yes)
                        {
                            frmAgregarNuevoAutor frm = new frmAgregarNuevoAutor(CONFIG, gestorLibro, datoNuevo);
                            frm.ShowDialog();
                            if (!frm.AUTORAGREGADO.Equals(""))
                            {
                                cargarCombosAutor();
                                libroActual.autores.Add(frm.AUTORAGREGADO);
                            }
                        }
                    }
                }
                recargarTblAutores(libroActual.autores);
            }
        }

        private void recargarTblAutores(List<string> lista)
        {
            tblAutores.Rows.Clear();
            tblAutores.Refresh();
            foreach (string d in lista)
            {
                tblAutores.Rows.Add(new object[] { d, 'x' });
            }
        }

        private void tbnAgregarCategoriaN_Click(object sender, EventArgs e)
        {
            agregarNuevoCategoriaEnTabla();
        }

        private void agregarNuevoCategoriaEnTabla()
        {
            string datoNuevo = cbCategoriasN.Text.Trim();
            if (libroActual.categorias == null)
            {
                libroActual.categorias = new List<string>();
            }

            if (!libroActual.categorias.Contains(datoNuevo))
            {
                libroActual.categorias.Add(datoNuevo);
            }
            recargarTblCategorias(libroActual.categorias);
        }

        private void tblCategorias_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == tblCategorias.Columns["controlCat"].Index)
            {
                libroActual.categorias.Remove(tblCategorias.Rows[e.RowIndex].Cells["nombre_categoria"].Value.ToString());
                recargarTblCategorias(libroActual.categorias);
            }
        }

        private void recargarTblCategorias(List<string> lista)
        {
            tblCategorias.Rows.Clear();
            tblCategorias.Refresh();
            foreach (string d in lista)
            {
                tblCategorias.Rows.Add(new object[] { d, 'x' });
            }
        }

        private void btnGuardarDatosLibro_Click(object sender, EventArgs e)
        {
            if (txtNombreLibroN.Text.Trim().Equals(""))
            {
                MessageBox.Show("Debe agregar un nombre!");
                return;
            }

            if (cbEditorialesN.Text.Trim().Equals(""))
            {
                MessageBox.Show("Debe agregar una Editorial!");
                return;
            }

            if (tblAutores.RowCount == 0)
            {
                MessageBox.Show("Debe agregar los Autores!");
                return;
            }

            if (tblCategorias.RowCount == 0)
            {
                MessageBox.Show("Debe agregar las Categorias!");
                return;
            }

            Modelo.Libro libro = new Modelo.Libro();
            libro.nombre_libro = txtNombreLibroN.Text.Trim();
            libro.isbn = txtIsbnN.Text.Trim();
            libro.editorial = cbEditorialesN.Text.Trim();
            libro.activo = checkActivoN.Checked;
            libro.autores = new List<string>();
            foreach (DataGridViewRow row in tblAutores.Rows)
            {
                libro.autores.Add(row.Cells["nombre_autor"].Value.ToString().Trim());
            }
            libro.categorias = new List<string>();
            foreach (DataGridViewRow row in tblCategorias.Rows)
            {
                libro.categorias.Add(row.Cells["nombre_categoria"].Value.ToString().Trim());
            }

            int resultado = gestorLibro.agregarNuevoLibro(libro);
            if (resultado == 1)
            {
                MessageBox.Show("Libro Guardado Exitosamente!");
                txtNombreLibroN.Text = "";
                txtIsbnN.Text = "";
                cbEditorialesN.Text = "";
                libroActual.categorias = new List<string>();
                libroActual.autores = new List<string>();
                tblAutores.Rows.Clear();
                tblCategorias.Rows.Clear();

                cargarCombosCategoria();
                cargarCombosEditorial();
                cargarCombosAutor();
            }
            else
            {
                MessageBox.Show("Error al Guardar los datos");
            }
        }

        private void cbAutoresN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                agregarNuevoAutorEnTabla();
            }
        }

        private void cbCategoriasN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                agregarNuevoCategoriaEnTabla();
            }
        }

        private void btnBusqueda_Click(object sender, EventArgs e)
        {
            buscarLibros();
        }

        private void buscarLibros()
        {
            List<Modelo.Libro> libros = gestorLibro.buscarLibros(txtTerminoBusqueda.Text, cbCategoriasB.Text, cbEditorialesB.Text, checkTitulo.Checked, checkAutor.Checked);//buscarLibros(fitro, categoria, editorial, nombre, autor);{

            tblLibrosResultado.Rows.Clear();
            tblLibrosResultado.Refresh();
            foreach (Modelo.Libro libro in libros)
            {
                tblLibrosResultado.Rows.Add(new object[] { libro.cod_libro, libro.nombre_libro, libro.editorial, String.Join("; ", libro.autores.ToArray()), String.Join("; ", libro.categorias.ToArray()) });
            }
        }

        private void cbCategoriasB_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cbCategoriasB.Text.Equals("TODAS") && !cbCategoriasB.Text.Trim().Equals(""))
                buscarLibros();
        }

        private void cbEditorialesB_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cbEditorialesB.Text.Equals("TODAS") && !cbEditorialesB.Text.Trim().Equals(""))
                buscarLibros();
        }

        private void txtTerminoBusqueda_TextChanged(object sender, EventArgs e)
        {
            if (!txtTerminoBusqueda.Text.Trim().Equals(""))
                buscarLibros();
            else
                tblLibrosResultado.Rows.Clear();
        }

        private void checkTitulo_CheckedChanged(object sender, EventArgs e)
        {
            buscarLibros();
        }

        private void checkAutor_CheckedChanged(object sender, EventArgs e)
        {
            buscarLibros();
        }

        private void tblLibrosResultado_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int pos = e.RowIndex;
            cargarInfoLibro(pos);
            btnActualizarLibro.Visible = true;

        }

        private void cargarInfoLibro(int pos)
        {
            try
            {
                int cod_libro = Int32.Parse(tblLibrosResultado.Rows[pos].Cells["cod_libro"].Value.ToString());
                Modelo.Libro libro = gestorLibro.cargarInfoLibro(cod_libro);
                libroActual = libro;

                txtNombreLibroN.Text = libro.nombre_libro;
                txtIsbnN.Text = libro.isbn;
                cbEditorialesN.Text = libro.editorial;
                recargarTblAutores(libro.autores);
                recargarTblCategorias(libro.categorias);
            }
            catch (Exception ex)
            {
            }
        }

        private void btnCancelarGuardado_Click(object sender, EventArgs e)
        {
            libroActual = new Modelo.Libro();
            txtNombreLibroN.Text = "";
            txtIsbnN.Text = "";
            cbEditorialesN.Text = "";
            recargarTblAutores(new List<string>());
            recargarTblCategorias(new List<string>());
            btnActualizarLibro.Visible = false;
        }

        private void btnActualizarLibro_Click(object sender, EventArgs e)
        {
            Modelo.Libro libro = new Modelo.Libro();
            libro.cod_libro = libroActual.cod_libro;
            libro.nombre_libro = txtNombreLibroN.Text.Trim();
            libro.isbn = txtIsbnN.Text.Trim();
            libro.editorial = cbEditorialesN.Text.Trim();
            libro.activo = checkActivoN.Checked;
            libro.autores = new List<string>();
            foreach (DataGridViewRow row in tblAutores.Rows)
            {
                libro.autores.Add(row.Cells["nombre_autor"].Value.ToString().Trim());
            }
            libro.categorias = new List<string>();
            foreach (DataGridViewRow row in tblCategorias.Rows)
            {
                libro.categorias.Add(row.Cells["nombre_categoria"].Value.ToString().Trim());
            }

            int resultado = gestorLibro.actualizarInfoLibro(libro);
            if (resultado == 1)
            {
                MessageBox.Show("Libro Guardado Exitosamente!");
                txtNombreLibroN.Text = "";
                txtIsbnN.Text = "";
                cbEditorialesN.Text = "";
                libroActual.categorias = new List<string>();
                libroActual.autores = new List<string>();
                tblAutores.Rows.Clear();
                tblCategorias.Rows.Clear();

                cargarCombosCategoria();
                cargarCombosEditorial();
                cargarCombosAutor();
                btnActualizarLibro.Visible = false;
            }
            else
            {
                MessageBox.Show("Error al Guardar los datos");
            }
        }

        private void btnSeleccionarLibro_Click(object sender, EventArgs e)
        {
            try
            {
                libroSeleccionadoPrestamo = gestorLibro.cargarInfoLibro(Int32.Parse(libroActual.cod_libro));
                if (libroSeleccionadoPrestamo.cod_libro.Equals(libroActual.cod_libro))
                {
                    this.Dispose();
                }
                else
                {
                    MessageBox.Show("Debe seleccionar un lector ya creado");
                }
            }
            catch (Exception ex)
            {

            }
        }

    }
}
