﻿using PostgresConexion;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Administrador_Biblioteca.Gestor
{
    public class PersonaDAO
    {
        string sql = "";
        PgConexion conn;

        public PersonaDAO (PgConexion conn) 
        {
            // TODO: Complete member initialization
            this.conn = conn;
        }

        public DataTable cargarTiposDocumentos()
        {
            DataTable dt = new DataTable();

            sql = "SELECT cod_documento, tipo_documento FROM t_tipo_documento order by 1;";

            dt = conn.ejecutaConsulta(sql);

            return dt;
        }

        internal List<Modelo.Persona> buscarPersonas(bool activos, bool todos, bool usuarios, string filtroDocumento, string filtroNombre, string filtroApellido)
        {
            List<Modelo.Persona> personasResultado = new List<Modelo.Persona>();
            DataTable dt = new DataTable();

            string strSqlFiltro = "";
            if (!todos)
            {
                if (activos)
                {
                    strSqlFiltro = " p.activo = 'S' ";
                }
                else
                {
                    strSqlFiltro = " p.activo = 'N' ";
                }
                if (usuarios)
                {
                    if (!strSqlFiltro.Trim().Equals(""))
                    {
                        strSqlFiltro += " and ";
                    }
                    strSqlFiltro += " u.activo = 'S' ";
                }
            }

            if (!filtroDocumento.Trim().Equals(""))
            {
                if (!strSqlFiltro.Trim().Equals(""))
                {
                    strSqlFiltro += " and ";
                }
                strSqlFiltro += " p.documento_persona like '%"+filtroDocumento+"%' ";
            }

            if (!filtroNombre.Trim().Equals(""))
            {
                if (!strSqlFiltro.Trim().Equals(""))
                {
                    strSqlFiltro += " and ";
                }
                strSqlFiltro += " upper(p.nombres) like upper('%" + filtroNombre + "%') ";
            }

            if (!filtroApellido.Trim().Equals(""))
            {
                if (!strSqlFiltro.Trim().Equals(""))
                {
                    strSqlFiltro += " and ";
                }
                strSqlFiltro += " upper(p.apellidos) like upper('%" + filtroApellido + "%') ";
            }



            if (!strSqlFiltro.Trim().Equals(""))
            {
                strSqlFiltro = " where  " + strSqlFiltro;
            }

            sql = "select td.iniciales, p.documento_persona, upper(p.nombres) nombres, upper(p.apellidos) apellidos, p.activo, coalesce(u.activo, 'N') usuario " +
                    " from t_persona p " +
                    " inner join t_tipo_documento td using(cod_documento) " +
                    " left join t_usuario u using(documento_persona) " +
                    strSqlFiltro +
                    " order by p.nombres, apellidos";

            dt = conn.ejecutaConsulta(sql);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Modelo.Persona persona = new Modelo.Persona();
                    persona.tipo_documento = row["iniciales"].ToString();
                    persona.documento_persona = row["documento_persona"].ToString();
                    persona.nombres = row["nombres"].ToString();
                    persona.apellidos = row["apellidos"].ToString();
                    persona.activo = row["activo"].ToString().Equals("S");
                    persona.usuario_activo = row["usuario"].ToString().Equals("S");
                    personasResultado.Add(persona);
                }
            }

            return personasResultado;
        }


        internal Modelo.Persona cargarInfoPersona(string documentoPersona)
        {
            Modelo.Persona persona = new Modelo.Persona();
            DataTable dt = new DataTable();

            sql = "select td.tipo_documento, p.cod_documento, p.documento_persona, upper(p.nombres) nombres, upper(p.apellidos) apellidos, " +
                    " p.telefono, p.email, p.activo, coalesce(u.nombre_usuario, '') nombre_usuario, coalesce(u.activo,'') usuario_activo, coalesce(u.super_usuario, '') usuario_administrador " +
                    " from t_persona p " +
                    " inner join t_tipo_documento td using(cod_documento) " +
                    " left join t_usuario u using(documento_persona) " +
                    " where p.documento_persona = '"+documentoPersona+"' " +
                    " order by p.nombres, apellidos";

            dt = conn.ejecutaConsulta(sql);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    persona.tipo_documento = row["tipo_documento"].ToString();
                    persona.documento_persona = row["documento_persona"].ToString();
                    persona.nombres = row["nombres"].ToString();
                    persona.apellidos = row["apellidos"].ToString();
                    persona.telefono = row["telefono"].ToString();
                    persona.email = row["email"].ToString();
                    persona.activo = row["activo"].ToString().Equals("S");
                    persona.usuario = row["nombre_usuario"].ToString();
                    persona.usuario_activo = row["usuario_activo"].ToString().Equals("S");
                    persona.usuario_administrador = row["usuario_administrador"].ToString().Equals("S");
                }
            }

            return persona;
        }

        internal bool agregarNuevaPersona(Modelo.Persona persona)
        {
            int resultado = 0;
            sql = "INSERT INTO t_persona( " +
                   "          cod_documento, documento_persona, nombres, apellidos, telefono,  " +
                   "          email, activo) " +
                   "  VALUES ((SELECT cod_documento FROM t_tipo_documento WHERE tipo_documento = '"+persona.tipo_documento+"'), '"+persona.documento_persona+"',  " +
                   "          '" + persona.nombres + "', '" + persona.apellidos + "', '" + persona.telefono + "', '" + persona.email + "', '" + (persona.activo? "S" : "N") + "');";
            resultado = conn.ejecutaDML(sql);
            if (resultado > 0)
                return true;
            return false;
        }


        internal bool actualizarPersona(Modelo.Persona persona)
        {
            int resultado = 0;
            sql = "UPDATE t_persona " +
                  "      SET cod_documento=(SELECT cod_documento FROM t_tipo_documento WHERE tipo_documento = '" + persona.tipo_documento + "'),   " +
                  "          nombres='" + persona.nombres + "', apellidos='" + persona.apellidos + "', telefono= '" + persona.telefono + "', email='" + persona.email + "', activo='" + (persona.activo ? "S" : "N") + "' " +
                  "    WHERE documento_persona= '" + persona.documento_persona + "';";

            resultado = conn.ejecutaDML(sql);
            if (resultado > 0)
                return true;
            return false;
        }
    }
}
