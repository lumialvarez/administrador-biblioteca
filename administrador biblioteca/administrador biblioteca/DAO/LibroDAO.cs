﻿using PostgresConexion;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_Biblioteca
{
    public class LibroDAO
    {
        private PgConexion conn;
        string sql = "";

        public LibroDAO(PostgresConexion.PgConexion conn)
        {
            this.conn = conn;
        }

        public DataTable cargarCategorias()
        {
            DataTable dt = new DataTable();

            sql = "select cod_categoria, nombre_categoria from t_categoria order by 2;";

            dt = conn.ejecutaConsulta(sql);

            return dt;
        }


        public DataTable cargarEditoriales()
        {
            DataTable dt = new DataTable();

            sql = "select cod_editorial, nombre_editorial from t_editorial order by 2;";

            dt = conn.ejecutaConsulta(sql);

            return dt;
        }

        public DataTable cargarAutores(string filtro)
        {
            DataTable dt = new DataTable();

            sql = "select cod_autor, nombre_autor, apellido_autor,  nombre_autor||', '||apellido_autor nombre_autor_completo from t_autor order by 2,3;";
     
            dt = conn.ejecutaConsulta(sql);

            return dt;
        }

        public bool agregarNuevoAutorInsert(string nombreAutor, string apellidoAutor)
        {
            sql = "INSERT INTO t_autor( nombre_autor, apellido_autor) " +
                " VALUES (upper('" + nombreAutor + "'), upper('" + apellidoAutor + "'));";

            int resultado = conn.ejecutaDML(sql);

            return (resultado > 0);
        }


        internal int agregarNuevoLibro(Modelo.Libro libro)
        {
            try
            {
                DataTable dt = new DataTable();
                conn.ejecutaConsulta("begin;");
                int cod_libro = -1;

                int cod_editorial = agregarNuevoEditorial(libro.editorial);
                if (cod_editorial == -1)
                {
                    throw new Exception("Error al obtener editorial");
                }

                sql = "INSERT INTO t_libro (nombre_libro, isbn, activo, cod_editorial) " +
                        "values (upper('" + libro.nombre_libro.Trim() + "'),'" + libro.isbn.Trim() + "','" + (libro.activo ? "S" : "N") + "'," + cod_editorial + ") RETURNING cod_libro;";
                dt = conn.ejecutaConsulta(sql);
                if (dt != null || dt.Rows.Count > 0)
                {
                    cod_libro = Int32.Parse(dt.Rows[0]["cod_libro"].ToString());
                }
                else
                {
                    throw new Exception("Error al insertar Libro");
                }

                foreach (string data in libro.autores)
                {
                    string nombre = "";
                    string apellido = "";
                    int cod_autor;

                    string[] datosNuevos = data.Split(new[] { ", " }, 2, StringSplitOptions.None);
                    if (datosNuevos != null && datosNuevos.Count() > 0)
                        nombre = datosNuevos[0].ToString();
                    if (datosNuevos != null && datosNuevos.Count() > 1)
                        apellido = datosNuevos[1].ToString();

                    cod_autor = agregarNuevoAutor(nombre, apellido);

                    sql = "INSERT INTO t_rel_libro_autor (cod_libro, cod_autor) " + 
                            " VALUES ("+cod_libro+","+cod_autor+")";
                    conn.ejecutaConsulta(sql);
                }

                foreach (string data in libro.categorias)
                {
                    int cod_categoria;
                    cod_categoria = agregarNuevaCategoria(data);

                    sql = "INSERT INTO t_rel_libro_categoria (cod_libro, cod_categoria) " +
                            " VALUES (" + cod_libro + "," + cod_categoria + ")";
                    conn.ejecutaConsulta(sql);
                }


                conn.ejecutaConsulta("commit;");
            }
            catch (Exception ex)
            {
                conn.ejecutaConsulta("rollback;");
                return 0;
            }

            return 1;
        }

        private int agregarNuevoEditorial(string nombre)
        {
            DataTable dt = new DataTable();
            int cod_editorial = -1;

            sql = "SELECT cod_editorial FROM t_editorial WHERE upper(nombre_editorial) = upper('" + nombre.Trim() +"');";
            dt = conn.ejecutaConsulta(sql);

            if (dt == null || dt.Rows.Count == 0)
            {
                sql = "INSERT INTO  t_editorial (nombre_editorial) VALUES (upper('" + nombre.Trim() + "')) RETURNING cod_editorial;";
                dt = conn.ejecutaConsulta(sql);

                cod_editorial = Int32.Parse(dt.Rows[0]["cod_editorial"].ToString());
            }
            else
            {
                cod_editorial = Int32.Parse(dt.Rows[0]["cod_editorial"].ToString());
            }

            return cod_editorial;
        }

        private int agregarNuevoAutor(string nombre, string apellido)
        {
            DataTable dt = new DataTable();
            int cod_autor = -1;

            sql = "SELECT cod_autor FROM t_autor WHERE upper(nombre_autor) = upper('" + nombre.Trim() + "') and  upper(apellido_autor) = upper('" + apellido.Trim() + "');";
            dt = conn.ejecutaConsulta(sql);

            if (dt == null || dt.Rows.Count == 0)
            {
                sql = "INSERT INTO t_autor (nombre_autor, apellido_autor) VALUES (upper('" + nombre.Trim() + "'), upper('" + apellido.Trim() + "')) RETURNING cod_autor;";
                dt = conn.ejecutaConsulta(sql);

                cod_autor = Int32.Parse(dt.Rows[0]["cod_autor"].ToString());
            }
            else
            {
                cod_autor = Int32.Parse(dt.Rows[0]["cod_autor"].ToString());
            }

            return cod_autor;
        }

        private int agregarNuevaCategoria(string nombre)
        {
            DataTable dt = new DataTable();
            int cod_categoria = -1;

            sql = "SELECT cod_categoria FROM t_categoria WHERE upper(nombre_categoria) = upper('" + nombre.Trim() + "');";
            dt = conn.ejecutaConsulta(sql);

            if (dt == null || dt.Rows.Count == 0)
            {
                sql = "INSERT INTO t_categoria (nombre_categoria) VALUES (upper('" + nombre.Trim() + "')) RETURNING cod_categoria;";
                dt = conn.ejecutaConsulta(sql);

                cod_categoria = Int32.Parse(dt.Rows[0]["cod_categoria"].ToString());
            }
            else
            {
                cod_categoria = Int32.Parse(dt.Rows[0]["cod_categoria"].ToString());
            }

            return cod_categoria;
        }

        internal List<Modelo.Libro> buscarLibros(string fitro, string categoria, string editorial, bool nombre, bool autor)
        {
            List<Modelo.Libro> librosResultado = new List<Modelo.Libro>();
            DataTable dt = new DataTable();

            string strSqlFiltro = "";
            if (nombre)
            {
                strSqlFiltro = " upper(nombre_libro) like upper('%" + fitro.Trim() + "%') ";
            }
            if (autor)
            {
                if (!strSqlFiltro.Trim().Equals(""))
                {
                    strSqlFiltro += " or ";
                }
                strSqlFiltro += " cod_libro in (SELECT cod_libro  FROM t_rel_libro_autor " +
                                    "INNER JOIN t_autor USING (cod_autor) WHERE upper(nombre_autor) like upper('%" + fitro.Trim() + "%') or upper(apellido_autor)  like upper('%" + fitro.Trim() + "%')) ";
            }
            if (!strSqlFiltro.Trim().Equals(""))
            {
                strSqlFiltro = " AND (" + strSqlFiltro + ")";
            }
            string strSqlFiltroC = "";
            if (!categoria.Equals("TODAS"))
            {
                strSqlFiltroC = " AND  cod_libro in (SELECT cod_libro  FROM t_rel_libro_categoria " +
                                    "INNER JOIN t_categoria USING (cod_categoria) WHERE upper(nombre_categoria) like upper('%" + categoria.Trim() + "%')) ";
            }

            string strSqlFiltroE = "";
            if (!editorial.Equals("TODAS"))
            {
                strSqlFiltroE = " AND  cod_editorial in (SELECT cod_editorial  FROM t_editorial " +
                                    " WHERE upper(nombre_editorial) like upper('%" + editorial.Trim() + "%')) ";
            }

            sql = "SELECT cod_libro FROM t_libro l " +
                    "INNER JOIN t_editorial USING (cod_editorial) " +
                    "WHERE l.activo = 'S' " + strSqlFiltro + strSqlFiltroC + strSqlFiltroE + 
                    " order by nombre_libro";
            dt = conn.ejecutaConsulta(sql);


            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Modelo.Libro libro = new Modelo.Libro();
                    int cod_libro = Int32.Parse(row["cod_libro"].ToString());
                    libro = cargarInfoLibro(cod_libro);
                    librosResultado.Add(libro);
                }
            }
            return librosResultado;
        }

        internal Modelo.Libro cargarInfoLibro(int cod_libro)
        {
            DataTable dt = new DataTable();
            sql = "SELECT cod_libro, nombre_libro, isbn, nombre_editorial FROM t_libro l " +
                    "INNER JOIN t_editorial USING (cod_editorial) " +
                    "WHERE l.activo = 'S' and cod_libro = " + cod_libro;
            dt = conn.ejecutaConsulta(sql);

            Modelo.Libro libro = new Modelo.Libro();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    
                    libro.cod_libro = row["cod_libro"].ToString();
                    libro.nombre_libro = row["nombre_libro"].ToString();
                    libro.isbn = row["isbn"].ToString();
                    libro.editorial = row["nombre_editorial"].ToString();

                    libro.autores = obtenerAutores(cod_libro);

                    libro.categorias = obtenerCategorias(cod_libro);
                }
            }
            return libro;
        }

        internal List<string> obtenerAutores(int cod_libro)
        {
            DataTable dtAutores = new DataTable();
            sql = "SELECT nombre_autor, apellido_autor, nombre_autor||', '||apellido_autor nombre_autor_completo  " +
                    "FROM t_rel_libro_autor  " +
                    "INNER JOIN t_autor USING (cod_autor) " +
                    "WHERE cod_libro = " + cod_libro + ";";
            dtAutores = conn.ejecutaConsulta(sql);
            List<string> lista = new List<string>();
            if (dtAutores != null && dtAutores.Rows.Count > 0)
            {
                foreach (DataRow rowAutor in dtAutores.Rows)
                {
                    lista.Add(rowAutor["nombre_autor_completo"].ToString());
                }
            }
            return lista;
        }

        internal List<string> obtenerCategorias(int cod_libro)
        {
            DataTable dtCategoria = new DataTable();
            sql = "SELECT nombre_categoria FROM t_rel_libro_categoria  " +
                    "INNER JOIN t_categoria USING (cod_categoria) " +
                    "WHERE cod_libro = " + cod_libro + ";";
            dtCategoria = conn.ejecutaConsulta(sql);
            List<string> lista = new List<string>();
            if (dtCategoria != null && dtCategoria.Rows.Count > 0)
            {
                foreach (DataRow rowCategoria in dtCategoria.Rows)
                {
                    lista.Add(rowCategoria["nombre_categoria"].ToString());
                }
            }
            return lista;
        }

        internal int actualizarInfoLibro(Modelo.Libro libro)
        {
            try
            {
                DataTable dt = new DataTable();
                conn.ejecutaConsulta("begin;");

                int cod_editorial = agregarNuevoEditorial(libro.editorial);
                if (cod_editorial == -1)
                {
                    throw new Exception("Error al obtener editorial");
                }

                sql = "UPDATE t_libro SET nombre_libro = upper('" + libro.nombre_libro.Trim() + "'), isbn = '" + libro.isbn.Trim() + "', " +
                    " activo = '"+(libro.activo? "S" : "N")+"', cod_editorial =  " + cod_editorial + " WHERE cod_libro = "+libro.cod_libro+";";
                dt = conn.ejecutaConsulta(sql);

                sql = "DELETE FROM t_rel_libro_autor WHERE cod_libro = "+libro.cod_libro+";";
                conn.ejecutaConsulta(sql);

                foreach (string data in libro.autores)
                {
                    string nombre = "";
                    string apellido = "";
                    int cod_autor;

                    string[] datosNuevos = data.Split(new[] { ", " }, 2, StringSplitOptions.None);
                    if (datosNuevos != null && datosNuevos.Count() > 0)
                        nombre = datosNuevos[0].ToString();
                    if (datosNuevos != null && datosNuevos.Count() > 1)
                        apellido = datosNuevos[1].ToString();

                    cod_autor = agregarNuevoAutor(nombre, apellido);

                    sql = "INSERT INTO t_rel_libro_autor (cod_libro, cod_autor) " + 
                            " VALUES ("+libro.cod_libro+","+cod_autor+")";
                    conn.ejecutaConsulta(sql);
                }

                sql = "DELETE FROM t_rel_libro_categoria WHERE cod_libro = "+libro.cod_libro+";";
                conn.ejecutaConsulta(sql);

                foreach (string data in libro.categorias)
                {
                    int cod_categoria;
                    cod_categoria = agregarNuevaCategoria(data);

                    sql = "INSERT INTO t_rel_libro_categoria (cod_libro, cod_categoria) " +
                            " VALUES (" + libro.cod_libro + "," + cod_categoria + ")";
                    conn.ejecutaConsulta(sql);
                }


                conn.ejecutaConsulta("commit;");
            }
            catch (Exception ex)
            {
                conn.ejecutaConsulta("rollback;");
                return 0;
            }

            return 1;
        }
    }
}
