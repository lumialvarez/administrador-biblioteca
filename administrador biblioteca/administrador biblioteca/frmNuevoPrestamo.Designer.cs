﻿namespace Administrador_Biblioteca
{
    partial class frmNuevoPrestamo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNuevoPrestamo));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnGuardarNuevoPrestamo = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.tblDetallePrestamoTemporal = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblCodigoLibroItem = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblAudorLibroItem = new System.Windows.Forms.Label();
            this.lblEditorialLibroItem = new System.Windows.Forms.Label();
            this.lblISBNLibroItem = new System.Windows.Forms.Label();
            this.lblNombreLibroItem = new System.Windows.Forms.Label();
            this.txtObservacionLibroItem = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.dtpFechaEsperadaLibroItem = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.nudCantidadLibroItem = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAgregarAListaTemporal = new System.Windows.Forms.Button();
            this.btnBuscarLibro = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblEstadoPersona = new System.Windows.Forms.Label();
            this.lblNombrePersona = new System.Windows.Forms.Label();
            this.lblDocumentoPersona = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSeleccionarPersona = new System.Windows.Forms.Button();
            this.cod_libro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombre_libro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecha_entrega_esperada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.observacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eliminar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblDetallePrestamoTemporal)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCantidadLibroItem)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnGuardarNuevoPrestamo);
            this.groupBox1.Controls.Add(this.btnCancelar);
            this.groupBox1.Controls.Add(this.tblDetallePrestamoTemporal);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(750, 485);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informacion Prestamo";
            // 
            // btnGuardarNuevoPrestamo
            // 
            this.btnGuardarNuevoPrestamo.Location = new System.Drawing.Point(475, 448);
            this.btnGuardarNuevoPrestamo.Name = "btnGuardarNuevoPrestamo";
            this.btnGuardarNuevoPrestamo.Size = new System.Drawing.Size(149, 23);
            this.btnGuardarNuevoPrestamo.TabIndex = 4;
            this.btnGuardarNuevoPrestamo.Text = "Guardar Nuevo Prestamo";
            this.btnGuardarNuevoPrestamo.UseVisualStyleBackColor = true;
            this.btnGuardarNuevoPrestamo.Click += new System.EventHandler(this.btnGuardarNuevoPrestamo_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(630, 448);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 3;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // tblDetallePrestamoTemporal
            // 
            this.tblDetallePrestamoTemporal.AllowUserToAddRows = false;
            this.tblDetallePrestamoTemporal.AllowUserToResizeRows = false;
            this.tblDetallePrestamoTemporal.BackgroundColor = System.Drawing.SystemColors.Control;
            this.tblDetallePrestamoTemporal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tblDetallePrestamoTemporal.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.tblDetallePrestamoTemporal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tblDetallePrestamoTemporal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cod_libro,
            this.nombre_libro,
            this.cantidad,
            this.fecha_entrega_esperada,
            this.observacion,
            this.eliminar});
            this.tblDetallePrestamoTemporal.EnableHeadersVisualStyles = false;
            this.tblDetallePrestamoTemporal.GridColor = System.Drawing.SystemColors.Control;
            this.tblDetallePrestamoTemporal.Location = new System.Drawing.Point(6, 318);
            this.tblDetallePrestamoTemporal.Name = "tblDetallePrestamoTemporal";
            this.tblDetallePrestamoTemporal.ReadOnly = true;
            this.tblDetallePrestamoTemporal.RowHeadersVisible = false;
            this.tblDetallePrestamoTemporal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tblDetallePrestamoTemporal.Size = new System.Drawing.Size(738, 124);
            this.tblDetallePrestamoTemporal.TabIndex = 2;
            this.tblDetallePrestamoTemporal.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tblDetallePrestamoTemporal_CellContentClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblCodigoLibroItem);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.lblAudorLibroItem);
            this.groupBox3.Controls.Add(this.lblEditorialLibroItem);
            this.groupBox3.Controls.Add(this.lblISBNLibroItem);
            this.groupBox3.Controls.Add(this.lblNombreLibroItem);
            this.groupBox3.Controls.Add(this.txtObservacionLibroItem);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.dtpFechaEsperadaLibroItem);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.nudCantidadLibroItem);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.btnAgregarAListaTemporal);
            this.groupBox3.Controls.Add(this.btnBuscarLibro);
            this.groupBox3.Location = new System.Drawing.Point(6, 150);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(738, 162);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Detalle Libro";
            // 
            // lblCodigoLibroItem
            // 
            this.lblCodigoLibroItem.AutoSize = true;
            this.lblCodigoLibroItem.Location = new System.Drawing.Point(138, 18);
            this.lblCodigoLibroItem.Name = "lblCodigoLibroItem";
            this.lblCodigoLibroItem.Size = new System.Drawing.Size(106, 13);
            this.lblCodigoLibroItem.TabIndex = 17;
            this.lblCodigoLibroItem.Text = "lblCodigoLibroItem";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(84, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Codigo:";
            // 
            // lblAudorLibroItem
            // 
            this.lblAudorLibroItem.AutoSize = true;
            this.lblAudorLibroItem.Location = new System.Drawing.Point(138, 103);
            this.lblAudorLibroItem.Name = "lblAudorLibroItem";
            this.lblAudorLibroItem.Size = new System.Drawing.Size(100, 13);
            this.lblAudorLibroItem.TabIndex = 15;
            this.lblAudorLibroItem.Text = "lblAudorLibroItem";
            // 
            // lblEditorialLibroItem
            // 
            this.lblEditorialLibroItem.AutoSize = true;
            this.lblEditorialLibroItem.Location = new System.Drawing.Point(138, 79);
            this.lblEditorialLibroItem.Name = "lblEditorialLibroItem";
            this.lblEditorialLibroItem.Size = new System.Drawing.Size(111, 13);
            this.lblEditorialLibroItem.TabIndex = 14;
            this.lblEditorialLibroItem.Text = "lblEditorialLibroItem";
            // 
            // lblISBNLibroItem
            // 
            this.lblISBNLibroItem.AutoSize = true;
            this.lblISBNLibroItem.Location = new System.Drawing.Point(138, 60);
            this.lblISBNLibroItem.Name = "lblISBNLibroItem";
            this.lblISBNLibroItem.Size = new System.Drawing.Size(92, 13);
            this.lblISBNLibroItem.TabIndex = 13;
            this.lblISBNLibroItem.Text = "lblISBNLibroItem";
            // 
            // lblNombreLibroItem
            // 
            this.lblNombreLibroItem.AutoSize = true;
            this.lblNombreLibroItem.Location = new System.Drawing.Point(138, 38);
            this.lblNombreLibroItem.Name = "lblNombreLibroItem";
            this.lblNombreLibroItem.Size = new System.Drawing.Size(109, 13);
            this.lblNombreLibroItem.TabIndex = 12;
            this.lblNombreLibroItem.Text = "lblNombreLibroItem";
            // 
            // txtObservacionLibroItem
            // 
            this.txtObservacionLibroItem.Location = new System.Drawing.Point(142, 119);
            this.txtObservacionLibroItem.Multiline = true;
            this.txtObservacionLibroItem.Name = "txtObservacionLibroItem";
            this.txtObservacionLibroItem.Size = new System.Drawing.Size(319, 37);
            this.txtObservacionLibroItem.TabIndex = 11;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(62, 122);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 13);
            this.label13.TabIndex = 10;
            this.label13.Text = "Observacion:";
            // 
            // dtpFechaEsperadaLibroItem
            // 
            this.dtpFechaEsperadaLibroItem.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaEsperadaLibroItem.Location = new System.Drawing.Point(574, 34);
            this.dtpFechaEsperadaLibroItem.Name = "dtpFechaEsperadaLibroItem";
            this.dtpFechaEsperadaLibroItem.Size = new System.Drawing.Size(125, 22);
            this.dtpFechaEsperadaLibroItem.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(485, 41);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Fecha Entrega:";
            // 
            // nudCantidadLibroItem
            // 
            this.nudCantidadLibroItem.Location = new System.Drawing.Point(574, 58);
            this.nudCantidadLibroItem.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudCantidadLibroItem.Name = "nudCantidadLibroItem";
            this.nudCantidadLibroItem.ReadOnly = true;
            this.nudCantidadLibroItem.Size = new System.Drawing.Size(125, 22);
            this.nudCantidadLibroItem.TabIndex = 7;
            this.nudCantidadLibroItem.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(511, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Cantidad:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(76, 103);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "Autor(es):";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(79, 79);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Editorial:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(98, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "ISBN:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(81, 38);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Nombre:";
            // 
            // btnAgregarAListaTemporal
            // 
            this.btnAgregarAListaTemporal.Location = new System.Drawing.Point(543, 132);
            this.btnAgregarAListaTemporal.Name = "btnAgregarAListaTemporal";
            this.btnAgregarAListaTemporal.Size = new System.Drawing.Size(156, 23);
            this.btnAgregarAListaTemporal.TabIndex = 1;
            this.btnAgregarAListaTemporal.Text = "Agregar Libro a Prestamo";
            this.btnAgregarAListaTemporal.UseVisualStyleBackColor = true;
            this.btnAgregarAListaTemporal.Click += new System.EventHandler(this.btnAgregarAListaTemporal_Click);
            // 
            // btnBuscarLibro
            // 
            this.btnBuscarLibro.Location = new System.Drawing.Point(543, 103);
            this.btnBuscarLibro.Name = "btnBuscarLibro";
            this.btnBuscarLibro.Size = new System.Drawing.Size(156, 23);
            this.btnBuscarLibro.TabIndex = 0;
            this.btnBuscarLibro.Text = "Buscar Libro";
            this.btnBuscarLibro.UseVisualStyleBackColor = true;
            this.btnBuscarLibro.Click += new System.EventHandler(this.btnBuscarLibro_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblEstadoPersona);
            this.groupBox2.Controls.Add(this.lblNombrePersona);
            this.groupBox2.Controls.Add(this.lblDocumentoPersona);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btnSeleccionarPersona);
            this.groupBox2.Location = new System.Drawing.Point(6, 21);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(738, 123);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Informacion Lector";
            // 
            // lblEstadoPersona
            // 
            this.lblEstadoPersona.AutoSize = true;
            this.lblEstadoPersona.Location = new System.Drawing.Point(142, 88);
            this.lblEstadoPersona.Name = "lblEstadoPersona";
            this.lblEstadoPersona.Size = new System.Drawing.Size(96, 13);
            this.lblEstadoPersona.TabIndex = 6;
            this.lblEstadoPersona.Text = "lblEstadoPersona";
            // 
            // lblNombrePersona
            // 
            this.lblNombrePersona.AutoSize = true;
            this.lblNombrePersona.Location = new System.Drawing.Point(142, 49);
            this.lblNombrePersona.Name = "lblNombrePersona";
            this.lblNombrePersona.Size = new System.Drawing.Size(102, 13);
            this.lblNombrePersona.TabIndex = 5;
            this.lblNombrePersona.Text = "lblNombrePersona";
            // 
            // lblDocumentoPersona
            // 
            this.lblDocumentoPersona.AutoSize = true;
            this.lblDocumentoPersona.Location = new System.Drawing.Point(142, 27);
            this.lblDocumentoPersona.Name = "lblDocumentoPersona";
            this.lblDocumentoPersona.Size = new System.Drawing.Size(121, 13);
            this.lblDocumentoPersona.TabIndex = 4;
            this.lblDocumentoPersona.Text = "lblDocumentoPersona";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(91, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Estado:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Documento:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(85, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nombre:";
            // 
            // btnSeleccionarPersona
            // 
            this.btnSeleccionarPersona.Location = new System.Drawing.Point(543, 83);
            this.btnSeleccionarPersona.Name = "btnSeleccionarPersona";
            this.btnSeleccionarPersona.Size = new System.Drawing.Size(156, 23);
            this.btnSeleccionarPersona.TabIndex = 0;
            this.btnSeleccionarPersona.Text = "Seleccionar Lector";
            this.btnSeleccionarPersona.UseVisualStyleBackColor = true;
            this.btnSeleccionarPersona.Click += new System.EventHandler(this.btnSeleccionarPersona_Click);
            // 
            // cod_libro
            // 
            this.cod_libro.HeaderText = "Cod.";
            this.cod_libro.Name = "cod_libro";
            this.cod_libro.ReadOnly = true;
            this.cod_libro.Width = 40;
            // 
            // nombre_libro
            // 
            this.nombre_libro.HeaderText = "Libro";
            this.nombre_libro.Name = "nombre_libro";
            this.nombre_libro.ReadOnly = true;
            this.nombre_libro.Width = 170;
            // 
            // cantidad
            // 
            this.cantidad.HeaderText = "Cantidad";
            this.cantidad.Name = "cantidad";
            this.cantidad.ReadOnly = true;
            this.cantidad.Width = 60;
            // 
            // fecha_entrega_esperada
            // 
            this.fecha_entrega_esperada.HeaderText = "Fecha Entrega";
            this.fecha_entrega_esperada.Name = "fecha_entrega_esperada";
            this.fecha_entrega_esperada.ReadOnly = true;
            this.fecha_entrega_esperada.Width = 120;
            // 
            // observacion
            // 
            this.observacion.HeaderText = "Observacion";
            this.observacion.Name = "observacion";
            this.observacion.ReadOnly = true;
            this.observacion.Width = 310;
            // 
            // eliminar
            // 
            this.eliminar.HeaderText = "";
            this.eliminar.Name = "eliminar";
            this.eliminar.ReadOnly = true;
            this.eliminar.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.eliminar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.eliminar.Width = 20;
            // 
            // frmNuevoPrestamo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 507);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmNuevoPrestamo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Crear prestamo";
            this.Load += new System.EventHandler(this.frmNuevoPrestamo_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblDetallePrestamoTemporal)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCantidadLibroItem)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblEstadoPersona;
        private System.Windows.Forms.Label lblNombrePersona;
        private System.Windows.Forms.Label lblDocumentoPersona;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSeleccionarPersona;
        private System.Windows.Forms.Button btnBuscarLibro;
        private System.Windows.Forms.Button btnAgregarAListaTemporal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown nudCantidadLibroItem;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpFechaEsperadaLibroItem;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtObservacionLibroItem;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblAudorLibroItem;
        private System.Windows.Forms.Label lblEditorialLibroItem;
        private System.Windows.Forms.Label lblISBNLibroItem;
        private System.Windows.Forms.Label lblNombreLibroItem;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardarNuevoPrestamo;
        private System.Windows.Forms.Label lblCodigoLibroItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn cod_libro;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre_libro;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecha_entrega_esperada;
        private System.Windows.Forms.DataGridViewTextBoxColumn observacion;
        private System.Windows.Forms.DataGridViewButtonColumn eliminar;
        private System.Windows.Forms.DataGridView tblDetallePrestamoTemporal;
    }
}