﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_Biblioteca.Modelo
{
    public class Persona
    {
        public string tipo_documento { get; set; }
        public string documento_persona { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string telefono { get; set; }
        public string email { get; set; }
        public bool activo { get; set; }
        public string usuario { get; set; }
        public bool usuario_activo { get; set; }
        public string ultimo_prestamo { get; set; }
        public bool usuario_administrador { get; set; }
    }
}
