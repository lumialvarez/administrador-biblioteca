﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_Biblioteca.Modelo
{
    public class Prestamo
    {
        public string documento_persona_responsable { get; set; }
        public Persona persona { get; set; }
        public List<PrestamoDetalle> detalle { get; set; }
    }
}
