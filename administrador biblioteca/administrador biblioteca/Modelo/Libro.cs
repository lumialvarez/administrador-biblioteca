﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_Biblioteca.Modelo
{
    public class Libro
    {
        public string cod_libro { get; set; }
        public string nombre_libro { get; set; }
        public string isbn { get; set; }
        public bool activo { get; set; }
        public string editorial { get; set; }
        public List<string> autores { get; set; }
        public List<string> categorias { get; set; }
    }
}
