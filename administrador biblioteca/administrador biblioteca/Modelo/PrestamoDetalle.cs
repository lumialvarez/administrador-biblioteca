﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_Biblioteca.Modelo
{
    public class PrestamoDetalle
    {
        public string cod_libro { get; set; }
        public string nombre_libro { get; set; }
        public string fecha_entrega_esperada { get; set; }
        public int cantidad { get; set; }
        public string observacion { get; set; }
    }
}
