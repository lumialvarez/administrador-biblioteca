﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_Biblioteca.Modelo
{
    public class Configuracion
    {
        public List<string> permisos { get; set; }
        public string cod_usuario_actual { get; set; }
        public string usuario_actual { get; set; }
        public string nombre_persona_usuario_actual { get; set; }
        public bool llamadoDesdePrestamo { get; set; }
    }
}
