﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Administrador_Biblioteca
{
    public partial class frmAgregarNuevoAutor : Form
    {
        private Modelo.Configuracion CONFIG;
        private GestorLibro gestor;
        private string datoNuevo;
        public string AUTORAGREGADO{get; set;}

        public frmAgregarNuevoAutor(Modelo.Configuracion CONFIG, GestorLibro gestor, string datoNuevo)
        {
            InitializeComponent();
            this.CONFIG = CONFIG;
            this.gestor = gestor;
            this.datoNuevo = datoNuevo;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            lblWarning.Text = "";
            if (cbNombreAutor.Text.Trim().Equals(""))
            {
                lblWarning.Text += "Ingrese el Nombre \n";
            }
            if (cbApellidoAutor.Text.Trim().Equals(""))
            {
                lblWarning.Text += "Ingrese el Apellido \n";
            }
            if (lblWarning.Text.Equals(""))
            {
                bool resultado = gestor.agregarNuevoAutorInsert(cbNombreAutor.Text.Trim(), cbApellidoAutor.Text.Trim());
                if (resultado)
                {
                    MessageBox.Show("Autor agregado!");
                    AUTORAGREGADO = cbNombreAutor.Text.Trim() + ", " + cbApellidoAutor.Text.Trim();
                    this.Close();
                }
            }
        }

        private void frmAgregarNuevoAutor_Load(object sender, EventArgs e)
        {
            try
            {
                DataTable dttConsulta = gestor.cargarAutores("");
                this.cbNombreAutor.DataSource = dttConsulta.DefaultView;
                this.cbNombreAutor.DisplayMember = "nombre_autor";

                this.cbApellidoAutor.DataSource = dttConsulta.DefaultView;
                this.cbApellidoAutor.DisplayMember = "apellido_autor";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            cbNombreAutor.Text = "";
            cbApellidoAutor.Text = "";

            AUTORAGREGADO = "";
            string[] datosNuevos = datoNuevo.Split(new[] { ", " }, 2, StringSplitOptions.None);
            if (datosNuevos != null && datosNuevos.Count() > 0)
                cbNombreAutor.Text = datosNuevos[0].ToString();
            if (datosNuevos != null && datosNuevos.Count() > 1)
                cbApellidoAutor.Text = datosNuevos[1].ToString();
        }
    }
}
