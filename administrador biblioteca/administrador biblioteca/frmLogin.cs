﻿using Administrador_Biblioteca.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Administrador_Biblioteca
{
    public partial class frmLogin : Form
    {
        GestorGeneral gestor;
        public Configuracion CONFIG = new Configuracion();
        private string[] args;

        public frmLogin()
        {
            InitializeComponent();
        }

        public frmLogin(string[] args)
        {
            InitializeComponent();
            gestor = new GestorGeneral(args[0], args[1], args[2], args[3], args[4], true);
            this.args = args;
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            iniciarSesion();
            
            
        }

        private void iniciarSesion()
        {
            lblWarning.Text = "";
            try
            {
                DataTable dt = gestor.autenticarUsuario(txtNomUsuario.Text, Utils.CalculateMD5Hash(txtContraUsuario.Text));

                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        CONFIG.cod_usuario_actual = row["documento_persona"].ToString();
                        CONFIG.nombre_persona_usuario_actual = row["nombre_persona"].ToString();
                        CONFIG.usuario_actual = txtNomUsuario.Text;
                        CONFIG.permisos = new List<string>();

                        if (row["super_usuario"].ToString().Equals("S"))
                        {
                            CONFIG.permisos.Add("SUPER");
                            CONFIG.permisos.Add("USUARIO");
                        }
                        else
                        {
                            CONFIG.permisos.Add("USUARIO");
                        }

                        this.Hide();
                        frmPrincipal frm = new frmPrincipal(args, gestor, CONFIG);
                        frm.Show();
                    }
                }
                else
                {
                    lblWarning.Text = "Usuario Invalido!!";
                }

            }
            catch (Exception ex)
            {
                lblWarning.Text = "Error: " + ex.Message;
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            if (!gestor.probarConexion())
            {
                lblWarning.Text = "Error de conexion a Base de Datos";
            }
        }
    }
}
