﻿using Administrador_Biblioteca.Gestor;
using Administrador_Biblioteca.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Administrador_Biblioteca
{
    public partial class frmAdministrarLector : Form
    {
        public Persona personaSeleccionadoPrestamo { get; set; }
        private Modelo.Configuracion CONFIG;
        private GestorPersona gestorPersona;
        private bool bloquearRefresco = false;
        private string guardar_o_actualizar_info_persona = "G"; //'G' para guardar y 'A' para actualizar informacion del lector

        public frmAdministrarLector(Modelo.Configuracion CONFIG, GestorPersona gestorPersona)
        {
            InitializeComponent();
            this.CONFIG = CONFIG;
            this.gestorPersona = gestorPersona;
        }

        private void frmAdministrarLector_Load(object sender, EventArgs e)
        {
            chbActivos.Checked = true;
            chbTodos.Checked = false;
            chbUsuarioSis.Checked = false;
            buscarPersona();
            cargarTiposDocumento();

            if (!CONFIG.permisos.Contains("SUPER"))
            {
                chbActivo.Enabled = false;
                btnGuardarActualizarUsuario.Enabled = false;
                btnNuevoActualizar.Enabled = false;
                chbUsuarioSis.Enabled = false;
            }

            if (!CONFIG.llamadoDesdePrestamo)
            {
                btnSeleccionarPersona.Visible = false;
            }
        }

        private void cargarTiposDocumento()
        {
            try
            {
                DataTable dttConsulta = gestorPersona.cargarTiposDocumentos();
                this.cbTipoDocumento.DataSource = dttConsulta.DefaultView;
                this.cbTipoDocumento.DisplayMember = "tipo_documento";
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e.Message);
            }
        }

        private void buscarPersona()
        {
            try
            {
                List<Modelo.Persona> personas = gestorPersona.buscarPersonas(chbActivos.Checked, chbTodos.Checked, chbUsuarioSis.Checked, txtFiltroDocumento.Text, txtFiltroNombre.Text, txtFiltroApellido.Text);

                tblPersonas.Rows.Clear();
                tblPersonas.Refresh();
                foreach (Modelo.Persona persona in personas)
                {
                    tblPersonas.Rows.Add(new object[] { persona.tipo_documento, persona.documento_persona, persona.nombres, persona.apellidos, persona.activo, persona.usuario_activo });
                }
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                bloquearRefresco = false;
            }
        }

        private void chbActivos_CheckedChanged(object sender, EventArgs e)
        {
            if (!bloquearRefresco)
            {
                bloquearRefresco = true;
                chbTodos.Checked = false;
                Application.DoEvents();
                buscarPersona();
            }
        }

        private void chbUsuarioSis_CheckedChanged(object sender, EventArgs e)
        {
            if (!bloquearRefresco)
            {
                bloquearRefresco = true;
                chbTodos.Checked = false;
                Application.DoEvents();
                buscarPersona();
            }
        }

        private void chbTodos_CheckedChanged(object sender, EventArgs e)
        {
            if (!bloquearRefresco)
            {
                bloquearRefresco = true;
                chbActivos.Checked = false;
                chbUsuarioSis.Checked = false;
                Application.DoEvents();
                buscarPersona();
            }
        }

        private void txtFiltroDocumento_TextChanged(object sender, EventArgs e)
        {
            buscarPersona();
        }

        private void txtFiltroNombre_TextChanged(object sender, EventArgs e)
        {
            buscarPersona();
        }

        private void txtFiltroApellido_TextChanged(object sender, EventArgs e)
        {
            buscarPersona();
        }

        private void tblPersonas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int pos = e.RowIndex;
            cargarInfoPersonaByPos(pos);
            btnNuevoActualizar.Text = "Actualizar";
            txtDocumentoPersona.ReadOnly = true;
            guardar_o_actualizar_info_persona = "A";
        }

        private void cargarInfoPersonaByPos(int pos)
        {
            string documentoPersona = tblPersonas.Rows[pos].Cells["documento_persona"].Value.ToString();
            cargarInfoPersona2(documentoPersona);
        }

        private void cargarInfoPersona2(string documentoPersona)
        {
            Modelo.Persona persona = gestorPersona.cargarInfoPersona(documentoPersona);
            txtDocumentoPersona.Text = persona.documento_persona;
            cbTipoDocumento.Text = persona.tipo_documento;
            txtNombre.Text = persona.nombres;
            txtApellido.Text = persona.apellidos;
            txtTelefono.Text = persona.telefono;
            txtEmail.Text = persona.email;
            chbActivo.Checked = persona.activo;
            if (persona.usuario.Trim().Equals(""))
            {
                lblMarcaUsuario.Text = "";
                btnGuardarActualizarUsuario.Text = "Asignar Nuevo Usuario";
            }
            else
            {
                btnGuardarActualizarUsuario.Text = "Cambiar Datos de Usuario";
                if (persona.usuario_activo)
                {
                    lblMarcaUsuario.Text = "Usuario (" + persona.usuario + ")";
                }
                else
                {
                    lblMarcaUsuario.Text = "Usuario (" + persona.usuario + ") Inactivo";
                }
            }
            btnGuardarActualizarUsuario.Enabled = true;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            btnNuevoActualizar.Text = "Guardar Nuevo";
            guardar_o_actualizar_info_persona = "G";
            txtDocumentoPersona.ReadOnly = false;
            txtDocumentoPersona.Text = "";
            txtNombre.Text = "";
            txtApellido.Text = "";
            txtTelefono.Text = "";
            txtEmail.Text = "";
            chbActivo.Checked = true;
            lblMarcaUsuario.Text = "";
            btnGuardarActualizarUsuario.Enabled = false;
        }

        private void btnNuevoActualizar_Click(object sender, EventArgs e)
        {
            if (txtDocumentoPersona.Text.Trim().Equals(""))
            {
                MessageBox.Show("Ingrese el Numero de documento!");
                return;
            }
            if (txtNombre.Text.Trim().Equals(""))
            {
                MessageBox.Show("Ingrese el Nombre!");
                return;
            }
            if (txtApellido.Text.Trim().Equals(""))
            {
                MessageBox.Show("Ingrese el Apellido!");
                return;
            }

            DialogResult resultado = MessageBox.Show("Desea " + (guardar_o_actualizar_info_persona.Equals("G")? "Guardar un nuevo Lector" : "Actualizar el Lector") + "?", "Desea continuar?",
                MessageBoxButtons.YesNo);

            if (resultado == DialogResult.Yes)
            {
                Modelo.Persona persona = new Modelo.Persona();

                persona.documento_persona = txtDocumentoPersona.Text;
                persona.tipo_documento = cbTipoDocumento.Text;
                persona.nombres = txtNombre.Text;
                persona.apellidos = txtApellido.Text;
                persona.telefono = txtTelefono.Text;
                persona.email = txtEmail.Text;
                persona.activo = chbActivo.Checked;

                bool resultadoOperacion = false;
                if (guardar_o_actualizar_info_persona.Equals("G"))
                {
                    resultadoOperacion = gestorPersona.agregarNuevaPersona(persona);
                }
                else
                {
                    resultadoOperacion = gestorPersona.actualizarPersona(persona);
                }

                if(resultadoOperacion){
                     MessageBox.Show( (guardar_o_actualizar_info_persona.Equals("G")? "Lector Agregado!" : "Lector Actualizado!"));
                     buscarPersona();
                     cargarInfoPersona2(txtDocumentoPersona.Text);
                     btnCancelar_Click(sender, e);
                }
                else
                {
                    MessageBox.Show((guardar_o_actualizar_info_persona.Equals("G") ? "No fue posible agregar nuevo Lector" : "No fue posible actualizar el Lector"));
                }
            }
        }

        private void btnGuardarActualizarUsuario_Click(object sender, EventArgs e)
        {
            if (guardar_o_actualizar_info_persona.Equals("A"))
            {
                try
                {
                    Modelo.Persona persona = gestorPersona.cargarInfoPersona(txtDocumentoPersona.Text);
                    frmAdministrarUsuario frm = new frmAdministrarUsuario(CONFIG, gestorPersona, persona);
                    frm.Show();
                }
                catch (Exception ex)
                {

                }
            }
            
        }

        private void btnSeleccionarPersona_Click(object sender, EventArgs e)
        {
            if (guardar_o_actualizar_info_persona.Equals("A"))
            {
                try
                {
                    personaSeleccionadoPrestamo = gestorPersona.cargarInfoPersona(txtDocumentoPersona.Text);
                    if (personaSeleccionadoPrestamo.documento_persona.Equals(txtDocumentoPersona.Text))
                    {
                        this.Dispose();
                    }
                }
                catch (Exception ex)
                {

                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un lector ya creado");
            }
        }


    }
}
