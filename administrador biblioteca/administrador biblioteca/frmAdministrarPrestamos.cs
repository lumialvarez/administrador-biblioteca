﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Administrador_Biblioteca
{
    public partial class frmAdministrarPrestamos : Form
    {
        private Modelo.Configuracion CONFIG;
        private Gestor.GestorPrestamo gestorPrestamo;

        public frmAdministrarPrestamos()
        {
            InitializeComponent();
        }

        public frmAdministrarPrestamos(Modelo.Configuracion CONFIG, Gestor.GestorPrestamo gestorPrestamo)
        {
            InitializeComponent();
            this.CONFIG = CONFIG;
            this.gestorPrestamo = gestorPrestamo;
        }
    }
}
