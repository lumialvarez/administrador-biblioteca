﻿namespace Administrador_Biblioteca
{
    partial class frmAdministrarLector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAdministrarLector));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chbTodos = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFiltroApellido = new System.Windows.Forms.TextBox();
            this.txtFiltroNombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFiltroDocumento = new System.Windows.Forms.TextBox();
            this.chbUsuarioSis = new System.Windows.Forms.CheckBox();
            this.chbActivos = new System.Windows.Forms.CheckBox();
            this.tblPersonas = new System.Windows.Forms.DataGridView();
            this.tipo_documento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.documento_persona = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombres = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apellidos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.persona_activo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.usuario_activo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnGuardarActualizarUsuario = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnNuevoActualizar = new System.Windows.Forms.Button();
            this.lblMarcaUsuario = new System.Windows.Forms.Label();
            this.chbActivo = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtApellido = new System.Windows.Forms.TextBox();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.txtDocumentoPersona = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.cbTipoDocumento = new System.Windows.Forms.ComboBox();
            this.btnSeleccionarPersona = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblPersonas)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chbTodos);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.chbUsuarioSis);
            this.groupBox1.Controls.Add(this.chbActivos);
            this.groupBox1.Controls.Add(this.tblPersonas);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(848, 244);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Busqueda";
            // 
            // chbTodos
            // 
            this.chbTodos.AutoSize = true;
            this.chbTodos.Location = new System.Drawing.Point(134, 19);
            this.chbTodos.Name = "chbTodos";
            this.chbTodos.Size = new System.Drawing.Size(57, 17);
            this.chbTodos.TabIndex = 9;
            this.chbTodos.Text = "Todos";
            this.chbTodos.UseVisualStyleBackColor = true;
            this.chbTodos.CheckedChanged += new System.EventHandler(this.chbTodos_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.txtFiltroApellido);
            this.groupBox3.Controls.Add(this.txtFiltroNombre);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.txtFiltroDocumento);
            this.groupBox3.Location = new System.Drawing.Point(6, 65);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(214, 146);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Filtrar Por";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Apellidos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nombres";
            // 
            // txtFiltroApellido
            // 
            this.txtFiltroApellido.Location = new System.Drawing.Point(6, 119);
            this.txtFiltroApellido.Name = "txtFiltroApellido";
            this.txtFiltroApellido.Size = new System.Drawing.Size(202, 22);
            this.txtFiltroApellido.TabIndex = 5;
            this.txtFiltroApellido.TextChanged += new System.EventHandler(this.txtFiltroApellido_TextChanged);
            // 
            // txtFiltroNombre
            // 
            this.txtFiltroNombre.Location = new System.Drawing.Point(6, 80);
            this.txtFiltroNombre.Name = "txtFiltroNombre";
            this.txtFiltroNombre.Size = new System.Drawing.Size(202, 22);
            this.txtFiltroNombre.TabIndex = 3;
            this.txtFiltroNombre.TextChanged += new System.EventHandler(this.txtFiltroNombre_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nro Documento";
            // 
            // txtFiltroDocumento
            // 
            this.txtFiltroDocumento.Location = new System.Drawing.Point(6, 41);
            this.txtFiltroDocumento.Name = "txtFiltroDocumento";
            this.txtFiltroDocumento.Size = new System.Drawing.Size(202, 22);
            this.txtFiltroDocumento.TabIndex = 1;
            this.txtFiltroDocumento.TextChanged += new System.EventHandler(this.txtFiltroDocumento_TextChanged);
            // 
            // chbUsuarioSis
            // 
            this.chbUsuarioSis.AutoSize = true;
            this.chbUsuarioSis.Location = new System.Drawing.Point(6, 42);
            this.chbUsuarioSis.Name = "chbUsuarioSis";
            this.chbUsuarioSis.Size = new System.Drawing.Size(127, 17);
            this.chbUsuarioSis.TabIndex = 7;
            this.chbUsuarioSis.Text = "Usuario del Sistema";
            this.chbUsuarioSis.UseVisualStyleBackColor = true;
            this.chbUsuarioSis.CheckedChanged += new System.EventHandler(this.chbUsuarioSis_CheckedChanged);
            // 
            // chbActivos
            // 
            this.chbActivos.AutoSize = true;
            this.chbActivos.Location = new System.Drawing.Point(6, 19);
            this.chbActivos.Name = "chbActivos";
            this.chbActivos.Size = new System.Drawing.Size(107, 17);
            this.chbActivos.TabIndex = 2;
            this.chbActivos.Text = "Lectores Activos";
            this.chbActivos.UseVisualStyleBackColor = true;
            this.chbActivos.CheckedChanged += new System.EventHandler(this.chbActivos_CheckedChanged);
            // 
            // tblPersonas
            // 
            this.tblPersonas.AllowUserToAddRows = false;
            this.tblPersonas.AllowUserToDeleteRows = false;
            this.tblPersonas.AllowUserToResizeRows = false;
            this.tblPersonas.BackgroundColor = System.Drawing.SystemColors.Control;
            this.tblPersonas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tblPersonas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tipo_documento,
            this.documento_persona,
            this.nombres,
            this.apellidos,
            this.persona_activo,
            this.usuario_activo});
            this.tblPersonas.EnableHeadersVisualStyles = false;
            this.tblPersonas.GridColor = System.Drawing.SystemColors.Control;
            this.tblPersonas.Location = new System.Drawing.Point(226, 19);
            this.tblPersonas.Name = "tblPersonas";
            this.tblPersonas.RowHeadersVisible = false;
            this.tblPersonas.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.tblPersonas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tblPersonas.Size = new System.Drawing.Size(619, 214);
            this.tblPersonas.TabIndex = 0;
            this.tblPersonas.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tblPersonas_CellClick);
            // 
            // tipo_documento
            // 
            this.tipo_documento.FillWeight = 50F;
            this.tipo_documento.HeaderText = "Tipo Documento";
            this.tipo_documento.Name = "tipo_documento";
            this.tipo_documento.ReadOnly = true;
            // 
            // documento_persona
            // 
            this.documento_persona.FillWeight = 70F;
            this.documento_persona.HeaderText = "Documento";
            this.documento_persona.Name = "documento_persona";
            this.documento_persona.ReadOnly = true;
            // 
            // nombres
            // 
            this.nombres.FillWeight = 200F;
            this.nombres.HeaderText = "nombres";
            this.nombres.Name = "nombres";
            this.nombres.ReadOnly = true;
            // 
            // apellidos
            // 
            this.apellidos.FillWeight = 200F;
            this.apellidos.HeaderText = "Apellidos";
            this.apellidos.Name = "apellidos";
            this.apellidos.ReadOnly = true;
            // 
            // persona_activo
            // 
            this.persona_activo.FillWeight = 50F;
            this.persona_activo.HeaderText = "Activo";
            this.persona_activo.Name = "persona_activo";
            this.persona_activo.ReadOnly = true;
            this.persona_activo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.persona_activo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // usuario_activo
            // 
            this.usuario_activo.FillWeight = 50F;
            this.usuario_activo.HeaderText = "Usuario Activo";
            this.usuario_activo.Name = "usuario_activo";
            this.usuario_activo.ReadOnly = true;
            this.usuario_activo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.usuario_activo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSeleccionarPersona);
            this.groupBox2.Controls.Add(this.btnGuardarActualizarUsuario);
            this.groupBox2.Controls.Add(this.btnCancelar);
            this.groupBox2.Controls.Add(this.btnNuevoActualizar);
            this.groupBox2.Controls.Add(this.lblMarcaUsuario);
            this.groupBox2.Controls.Add(this.chbActivo);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtEmail);
            this.groupBox2.Controls.Add(this.txtApellido);
            this.groupBox2.Controls.Add(this.txtTelefono);
            this.groupBox2.Controls.Add(this.txtDocumentoPersona);
            this.groupBox2.Controls.Add(this.txtNombre);
            this.groupBox2.Controls.Add(this.cbTipoDocumento);
            this.groupBox2.Location = new System.Drawing.Point(12, 257);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(848, 203);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Detalle";
            // 
            // btnGuardarActualizarUsuario
            // 
            this.btnGuardarActualizarUsuario.Enabled = false;
            this.btnGuardarActualizarUsuario.Location = new System.Drawing.Point(505, 118);
            this.btnGuardarActualizarUsuario.Name = "btnGuardarActualizarUsuario";
            this.btnGuardarActualizarUsuario.Size = new System.Drawing.Size(216, 23);
            this.btnGuardarActualizarUsuario.TabIndex = 17;
            this.btnGuardarActualizarUsuario.Text = "Crear Usuario";
            this.btnGuardarActualizarUsuario.UseVisualStyleBackColor = true;
            this.btnGuardarActualizarUsuario.Click += new System.EventHandler(this.btnGuardarActualizarUsuario_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(617, 172);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(104, 23);
            this.btnCancelar.TabIndex = 16;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnNuevoActualizar
            // 
            this.btnNuevoActualizar.Location = new System.Drawing.Point(505, 172);
            this.btnNuevoActualizar.Name = "btnNuevoActualizar";
            this.btnNuevoActualizar.Size = new System.Drawing.Size(102, 23);
            this.btnNuevoActualizar.TabIndex = 15;
            this.btnNuevoActualizar.Text = "Guardar Nuevo";
            this.btnNuevoActualizar.UseVisualStyleBackColor = true;
            this.btnNuevoActualizar.Click += new System.EventHandler(this.btnNuevoActualizar_Click);
            // 
            // lblMarcaUsuario
            // 
            this.lblMarcaUsuario.AutoSize = true;
            this.lblMarcaUsuario.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMarcaUsuario.Location = new System.Drawing.Point(256, 118);
            this.lblMarcaUsuario.Name = "lblMarcaUsuario";
            this.lblMarcaUsuario.Size = new System.Drawing.Size(80, 13);
            this.lblMarcaUsuario.TabIndex = 14;
            this.lblMarcaUsuario.Text = "Marca Usuario";
            // 
            // chbActivo
            // 
            this.chbActivo.AutoSize = true;
            this.chbActivo.Location = new System.Drawing.Point(178, 117);
            this.chbActivo.Name = "chbActivo";
            this.chbActivo.Size = new System.Drawing.Size(57, 17);
            this.chbActivo.TabIndex = 13;
            this.chbActivo.Text = "Activo";
            this.chbActivo.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(86, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Tipo Documento";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(117, 64);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Nombre(s)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(444, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Apellido(s)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(123, 92);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Telefono";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(467, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Email";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(417, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Nro Documento";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(505, 89);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(216, 22);
            this.txtEmail.TabIndex = 6;
            // 
            // txtApellido
            // 
            this.txtApellido.Location = new System.Drawing.Point(505, 61);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Size = new System.Drawing.Size(216, 22);
            this.txtApellido.TabIndex = 4;
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(178, 89);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(219, 22);
            this.txtTelefono.TabIndex = 3;
            // 
            // txtDocumentoPersona
            // 
            this.txtDocumentoPersona.Location = new System.Drawing.Point(505, 33);
            this.txtDocumentoPersona.Name = "txtDocumentoPersona";
            this.txtDocumentoPersona.Size = new System.Drawing.Size(216, 22);
            this.txtDocumentoPersona.TabIndex = 2;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(178, 61);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(219, 22);
            this.txtNombre.TabIndex = 1;
            // 
            // cbTipoDocumento
            // 
            this.cbTipoDocumento.FormattingEnabled = true;
            this.cbTipoDocumento.Location = new System.Drawing.Point(178, 34);
            this.cbTipoDocumento.Name = "cbTipoDocumento";
            this.cbTipoDocumento.Size = new System.Drawing.Size(219, 21);
            this.cbTipoDocumento.TabIndex = 0;
            // 
            // btnSeleccionarPersona
            // 
            this.btnSeleccionarPersona.Location = new System.Drawing.Point(178, 172);
            this.btnSeleccionarPersona.Name = "btnSeleccionarPersona";
            this.btnSeleccionarPersona.Size = new System.Drawing.Size(219, 23);
            this.btnSeleccionarPersona.TabIndex = 18;
            this.btnSeleccionarPersona.Text = "Seleccionar Lector";
            this.btnSeleccionarPersona.UseVisualStyleBackColor = true;
            this.btnSeleccionarPersona.Click += new System.EventHandler(this.btnSeleccionarPersona_Click);
            // 
            // frmAdministrarLector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 484);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAdministrarLector";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Aministracion de Lectores";
            this.Load += new System.EventHandler(this.frmAdministrarLector_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblPersonas)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView tblPersonas;
        private System.Windows.Forms.TextBox txtFiltroDocumento;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox chbUsuarioSis;
        private System.Windows.Forms.CheckBox chbActivos;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFiltroApellido;
        private System.Windows.Forms.TextBox txtFiltroNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chbTodos;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.TextBox txtDocumentoPersona;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.ComboBox cbTipoDocumento;
        private System.Windows.Forms.CheckBox chbActivo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblMarcaUsuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipo_documento;
        private System.Windows.Forms.DataGridViewTextBoxColumn documento_persona;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombres;
        private System.Windows.Forms.DataGridViewTextBoxColumn apellidos;
        private System.Windows.Forms.DataGridViewCheckBoxColumn persona_activo;
        private System.Windows.Forms.DataGridViewCheckBoxColumn usuario_activo;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnNuevoActualizar;
        private System.Windows.Forms.Button btnGuardarActualizarUsuario;
        private System.Windows.Forms.Button btnSeleccionarPersona;
    }
}