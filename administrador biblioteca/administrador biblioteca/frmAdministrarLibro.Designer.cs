﻿namespace Administrador_Biblioteca
{
    partial class frmAdministrarLibro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAdministrarLibro));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCancelarGuardado = new System.Windows.Forms.Button();
            this.btnActualizarLibro = new System.Windows.Forms.Button();
            this.cbEditorialesN = new System.Windows.Forms.ComboBox();
            this.btnGuardarDatosLibro = new System.Windows.Forms.Button();
            this.btnAgregarCategoriaN = new System.Windows.Forms.Button();
            this.btnAgregarAutorN = new System.Windows.Forms.Button();
            this.tblCategorias = new System.Windows.Forms.DataGridView();
            this.nombre_categoria = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.controlCat = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cbCategoriasN = new System.Windows.Forms.ComboBox();
            this.tblAutores = new System.Windows.Forms.DataGridView();
            this.nombre_autor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.control = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cbAutoresN = new System.Windows.Forms.ComboBox();
            this.checkActivoN = new System.Windows.Forms.CheckBox();
            this.txtIsbnN = new System.Windows.Forms.TextBox();
            this.txtNombreLibroN = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkAutor = new System.Windows.Forms.CheckBox();
            this.checkTitulo = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tblLibrosResultado = new System.Windows.Forms.DataGridView();
            this.cod_libro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombre_libro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.editorial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.autores = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categorias = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnBusqueda = new System.Windows.Forms.Button();
            this.txtTerminoBusqueda = new System.Windows.Forms.TextBox();
            this.cbEditorialesB = new System.Windows.Forms.ComboBox();
            this.cbCategoriasB = new System.Windows.Forms.ComboBox();
            this.btnSeleccionarLibro = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblCategorias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAutores)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblLibrosResultado)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSeleccionarLibro);
            this.groupBox1.Controls.Add(this.btnCancelarGuardado);
            this.groupBox1.Controls.Add(this.btnActualizarLibro);
            this.groupBox1.Controls.Add(this.cbEditorialesN);
            this.groupBox1.Controls.Add(this.btnGuardarDatosLibro);
            this.groupBox1.Controls.Add(this.btnAgregarCategoriaN);
            this.groupBox1.Controls.Add(this.btnAgregarAutorN);
            this.groupBox1.Controls.Add(this.tblCategorias);
            this.groupBox1.Controls.Add(this.cbCategoriasN);
            this.groupBox1.Controls.Add(this.tblAutores);
            this.groupBox1.Controls.Add(this.cbAutoresN);
            this.groupBox1.Controls.Add(this.checkActivoN);
            this.groupBox1.Controls.Add(this.txtIsbnN);
            this.groupBox1.Controls.Add(this.txtNombreLibroN);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 223);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(820, 226);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalle";
            // 
            // btnCancelarGuardado
            // 
            this.btnCancelarGuardado.Location = new System.Drawing.Point(691, 197);
            this.btnCancelarGuardado.Name = "btnCancelarGuardado";
            this.btnCancelarGuardado.Size = new System.Drawing.Size(95, 23);
            this.btnCancelarGuardado.TabIndex = 104;
            this.btnCancelarGuardado.Text = "Cancelar";
            this.btnCancelarGuardado.UseVisualStyleBackColor = true;
            this.btnCancelarGuardado.Click += new System.EventHandler(this.btnCancelarGuardado_Click);
            // 
            // btnActualizarLibro
            // 
            this.btnActualizarLibro.Location = new System.Drawing.Point(489, 197);
            this.btnActualizarLibro.Name = "btnActualizarLibro";
            this.btnActualizarLibro.Size = new System.Drawing.Size(95, 23);
            this.btnActualizarLibro.TabIndex = 103;
            this.btnActualizarLibro.Text = "Actualizar";
            this.btnActualizarLibro.UseVisualStyleBackColor = true;
            this.btnActualizarLibro.Click += new System.EventHandler(this.btnActualizarLibro_Click);
            // 
            // cbEditorialesN
            // 
            this.cbEditorialesN.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbEditorialesN.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbEditorialesN.FormattingEnabled = true;
            this.cbEditorialesN.Location = new System.Drawing.Point(75, 71);
            this.cbEditorialesN.Name = "cbEditorialesN";
            this.cbEditorialesN.Size = new System.Drawing.Size(228, 21);
            this.cbEditorialesN.TabIndex = 12;
            // 
            // btnGuardarDatosLibro
            // 
            this.btnGuardarDatosLibro.Location = new System.Drawing.Point(590, 197);
            this.btnGuardarDatosLibro.Name = "btnGuardarDatosLibro";
            this.btnGuardarDatosLibro.Size = new System.Drawing.Size(95, 23);
            this.btnGuardarDatosLibro.TabIndex = 16;
            this.btnGuardarDatosLibro.Text = "Guardar Nuevo";
            this.btnGuardarDatosLibro.UseVisualStyleBackColor = true;
            this.btnGuardarDatosLibro.Click += new System.EventHandler(this.btnGuardarDatosLibro_Click);
            // 
            // btnAgregarCategoriaN
            // 
            this.btnAgregarCategoriaN.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarCategoriaN.Location = new System.Drawing.Point(766, 15);
            this.btnAgregarCategoriaN.Name = "btnAgregarCategoriaN";
            this.btnAgregarCategoriaN.Size = new System.Drawing.Size(20, 23);
            this.btnAgregarCategoriaN.TabIndex = 102;
            this.btnAgregarCategoriaN.TabStop = false;
            this.btnAgregarCategoriaN.Text = "+";
            this.btnAgregarCategoriaN.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAgregarCategoriaN.UseVisualStyleBackColor = true;
            this.btnAgregarCategoriaN.Click += new System.EventHandler(this.tbnAgregarCategoriaN_Click);
            // 
            // btnAgregarAutorN
            // 
            this.btnAgregarAutorN.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarAutorN.Location = new System.Drawing.Point(530, 13);
            this.btnAgregarAutorN.Name = "btnAgregarAutorN";
            this.btnAgregarAutorN.Size = new System.Drawing.Size(20, 23);
            this.btnAgregarAutorN.TabIndex = 101;
            this.btnAgregarAutorN.TabStop = false;
            this.btnAgregarAutorN.Text = "+";
            this.btnAgregarAutorN.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAgregarAutorN.UseVisualStyleBackColor = true;
            this.btnAgregarAutorN.Click += new System.EventHandler(this.btnAgregarAutorN_Click);
            // 
            // tblCategorias
            // 
            this.tblCategorias.AllowUserToAddRows = false;
            this.tblCategorias.BackgroundColor = System.Drawing.SystemColors.Control;
            this.tblCategorias.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tblCategorias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tblCategorias.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nombre_categoria,
            this.controlCat});
            this.tblCategorias.GridColor = System.Drawing.SystemColors.Control;
            this.tblCategorias.Location = new System.Drawing.Point(556, 42);
            this.tblCategorias.MultiSelect = false;
            this.tblCategorias.Name = "tblCategorias";
            this.tblCategorias.ReadOnly = true;
            this.tblCategorias.RowHeadersVisible = false;
            this.tblCategorias.Size = new System.Drawing.Size(230, 149);
            this.tblCategorias.TabIndex = 100;
            this.tblCategorias.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tblCategorias_CellContentClick);
            // 
            // nombre_categoria
            // 
            this.nombre_categoria.HeaderText = "Categoria(s)";
            this.nombre_categoria.Name = "nombre_categoria";
            this.nombre_categoria.ReadOnly = true;
            this.nombre_categoria.Width = 200;
            // 
            // controlCat
            // 
            this.controlCat.HeaderText = "";
            this.controlCat.Name = "controlCat";
            this.controlCat.ReadOnly = true;
            this.controlCat.Width = 20;
            // 
            // cbCategoriasN
            // 
            this.cbCategoriasN.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbCategoriasN.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCategoriasN.FormattingEnabled = true;
            this.cbCategoriasN.Location = new System.Drawing.Point(556, 15);
            this.cbCategoriasN.Name = "cbCategoriasN";
            this.cbCategoriasN.Size = new System.Drawing.Size(204, 21);
            this.cbCategoriasN.TabIndex = 15;
            this.cbCategoriasN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbCategoriasN_KeyDown);
            // 
            // tblAutores
            // 
            this.tblAutores.AllowUserToAddRows = false;
            this.tblAutores.BackgroundColor = System.Drawing.SystemColors.Control;
            this.tblAutores.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tblAutores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tblAutores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nombre_autor,
            this.control});
            this.tblAutores.GridColor = System.Drawing.SystemColors.Control;
            this.tblAutores.Location = new System.Drawing.Point(325, 42);
            this.tblAutores.MultiSelect = false;
            this.tblAutores.Name = "tblAutores";
            this.tblAutores.ReadOnly = true;
            this.tblAutores.RowHeadersVisible = false;
            this.tblAutores.Size = new System.Drawing.Size(225, 149);
            this.tblAutores.TabIndex = 99;
            this.tblAutores.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tblAutores_CellContentClick);
            // 
            // nombre_autor
            // 
            this.nombre_autor.HeaderText = "Autor(es)";
            this.nombre_autor.Name = "nombre_autor";
            this.nombre_autor.ReadOnly = true;
            this.nombre_autor.Width = 200;
            // 
            // control
            // 
            this.control.HeaderText = "";
            this.control.Name = "control";
            this.control.ReadOnly = true;
            this.control.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.control.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.control.Width = 20;
            // 
            // cbAutoresN
            // 
            this.cbAutoresN.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbAutoresN.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbAutoresN.FormattingEnabled = true;
            this.cbAutoresN.Location = new System.Drawing.Point(325, 15);
            this.cbAutoresN.Name = "cbAutoresN";
            this.cbAutoresN.Size = new System.Drawing.Size(199, 21);
            this.cbAutoresN.TabIndex = 14;
            this.cbAutoresN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbAutoresN_KeyDown);
            // 
            // checkActivoN
            // 
            this.checkActivoN.AutoSize = true;
            this.checkActivoN.Checked = true;
            this.checkActivoN.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkActivoN.Location = new System.Drawing.Point(75, 99);
            this.checkActivoN.Name = "checkActivoN";
            this.checkActivoN.Size = new System.Drawing.Size(57, 17);
            this.checkActivoN.TabIndex = 13;
            this.checkActivoN.Text = "Activo";
            this.checkActivoN.UseVisualStyleBackColor = true;
            // 
            // txtIsbnN
            // 
            this.txtIsbnN.Location = new System.Drawing.Point(75, 43);
            this.txtIsbnN.Name = "txtIsbnN";
            this.txtIsbnN.Size = new System.Drawing.Size(228, 22);
            this.txtIsbnN.TabIndex = 11;
            // 
            // txtNombreLibroN
            // 
            this.txtNombreLibroN.Location = new System.Drawing.Point(75, 15);
            this.txtNombreLibroN.Name = "txtNombreLibroN";
            this.txtNombreLibroN.Size = new System.Drawing.Size(228, 22);
            this.txtNombreLibroN.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Editorial";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "ISBN";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Nombre";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.checkAutor);
            this.groupBox2.Controls.Add(this.checkTitulo);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.tblLibrosResultado);
            this.groupBox2.Controls.Add(this.btnBusqueda);
            this.groupBox2.Controls.Add(this.txtTerminoBusqueda);
            this.groupBox2.Controls.Add(this.cbEditorialesB);
            this.groupBox2.Controls.Add(this.cbCategoriasB);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(820, 205);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Busqueda";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Filtro Busqueda";
            // 
            // checkAutor
            // 
            this.checkAutor.AutoSize = true;
            this.checkAutor.Location = new System.Drawing.Point(100, 138);
            this.checkAutor.Name = "checkAutor";
            this.checkAutor.Size = new System.Drawing.Size(55, 17);
            this.checkAutor.TabIndex = 4;
            this.checkAutor.Text = "Autor";
            this.checkAutor.UseVisualStyleBackColor = true;
            this.checkAutor.CheckedChanged += new System.EventHandler(this.checkAutor_CheckedChanged);
            // 
            // checkTitulo
            // 
            this.checkTitulo.AutoSize = true;
            this.checkTitulo.Checked = true;
            this.checkTitulo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkTitulo.Location = new System.Drawing.Point(23, 138);
            this.checkTitulo.Name = "checkTitulo";
            this.checkTitulo.Size = new System.Drawing.Size(55, 17);
            this.checkTitulo.TabIndex = 3;
            this.checkTitulo.Text = "Titulo";
            this.checkTitulo.UseVisualStyleBackColor = true;
            this.checkTitulo.CheckedChanged += new System.EventHandler(this.checkTitulo_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Editorial";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Categoria";
            // 
            // tblLibrosResultado
            // 
            this.tblLibrosResultado.AllowUserToAddRows = false;
            this.tblLibrosResultado.AllowUserToDeleteRows = false;
            this.tblLibrosResultado.AllowUserToResizeRows = false;
            this.tblLibrosResultado.BackgroundColor = System.Drawing.SystemColors.Control;
            this.tblLibrosResultado.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tblLibrosResultado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tblLibrosResultado.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cod_libro,
            this.nombre_libro,
            this.editorial,
            this.autores,
            this.categorias});
            this.tblLibrosResultado.EnableHeadersVisualStyles = false;
            this.tblLibrosResultado.GridColor = System.Drawing.SystemColors.Control;
            this.tblLibrosResultado.Location = new System.Drawing.Point(298, 19);
            this.tblLibrosResultado.MultiSelect = false;
            this.tblLibrosResultado.Name = "tblLibrosResultado";
            this.tblLibrosResultado.ReadOnly = true;
            this.tblLibrosResultado.RowHeadersVisible = false;
            this.tblLibrosResultado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tblLibrosResultado.Size = new System.Drawing.Size(516, 166);
            this.tblLibrosResultado.TabIndex = 6;
            this.tblLibrosResultado.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tblLibrosResultado_CellClick);
            // 
            // cod_libro
            // 
            this.cod_libro.HeaderText = "Codigo";
            this.cod_libro.Name = "cod_libro";
            this.cod_libro.ReadOnly = true;
            this.cod_libro.Width = 50;
            // 
            // nombre_libro
            // 
            this.nombre_libro.HeaderText = "Nombre";
            this.nombre_libro.Name = "nombre_libro";
            this.nombre_libro.ReadOnly = true;
            this.nombre_libro.Width = 120;
            // 
            // editorial
            // 
            this.editorial.HeaderText = "Editorial";
            this.editorial.Name = "editorial";
            this.editorial.ReadOnly = true;
            // 
            // autores
            // 
            this.autores.HeaderText = "Autor(es)";
            this.autores.Name = "autores";
            this.autores.ReadOnly = true;
            this.autores.Width = 110;
            // 
            // categorias
            // 
            this.categorias.HeaderText = "Cateogoria(s)";
            this.categorias.Name = "categorias";
            this.categorias.ReadOnly = true;
            this.categorias.Width = 110;
            // 
            // btnBusqueda
            // 
            this.btnBusqueda.Location = new System.Drawing.Point(208, 162);
            this.btnBusqueda.Name = "btnBusqueda";
            this.btnBusqueda.Size = new System.Drawing.Size(75, 23);
            this.btnBusqueda.TabIndex = 5;
            this.btnBusqueda.Text = "Buscar";
            this.btnBusqueda.UseVisualStyleBackColor = true;
            this.btnBusqueda.Click += new System.EventHandler(this.btnBusqueda_Click);
            // 
            // txtTerminoBusqueda
            // 
            this.txtTerminoBusqueda.Location = new System.Drawing.Point(23, 112);
            this.txtTerminoBusqueda.Name = "txtTerminoBusqueda";
            this.txtTerminoBusqueda.Size = new System.Drawing.Size(260, 22);
            this.txtTerminoBusqueda.TabIndex = 2;
            this.txtTerminoBusqueda.TextChanged += new System.EventHandler(this.txtTerminoBusqueda_TextChanged);
            // 
            // cbEditorialesB
            // 
            this.cbEditorialesB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbEditorialesB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbEditorialesB.FormattingEnabled = true;
            this.cbEditorialesB.Location = new System.Drawing.Point(25, 72);
            this.cbEditorialesB.Name = "cbEditorialesB";
            this.cbEditorialesB.Size = new System.Drawing.Size(258, 21);
            this.cbEditorialesB.TabIndex = 1;
            this.cbEditorialesB.SelectedIndexChanged += new System.EventHandler(this.cbEditorialesB_SelectedIndexChanged);
            // 
            // cbCategoriasB
            // 
            this.cbCategoriasB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbCategoriasB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCategoriasB.FormattingEnabled = true;
            this.cbCategoriasB.Location = new System.Drawing.Point(25, 32);
            this.cbCategoriasB.Name = "cbCategoriasB";
            this.cbCategoriasB.Size = new System.Drawing.Size(258, 21);
            this.cbCategoriasB.TabIndex = 0;
            this.cbCategoriasB.SelectedIndexChanged += new System.EventHandler(this.cbCategoriasB_SelectedIndexChanged);
            // 
            // btnSeleccionarLibro
            // 
            this.btnSeleccionarLibro.Location = new System.Drawing.Point(90, 197);
            this.btnSeleccionarLibro.Name = "btnSeleccionarLibro";
            this.btnSeleccionarLibro.Size = new System.Drawing.Size(193, 23);
            this.btnSeleccionarLibro.TabIndex = 105;
            this.btnSeleccionarLibro.Text = "Seleccionar Libro";
            this.btnSeleccionarLibro.UseVisualStyleBackColor = true;
            this.btnSeleccionarLibro.Click += new System.EventHandler(this.btnSeleccionarLibro_Click);
            // 
            // frmAdministrarLibro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 461);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmAdministrarLibro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administrar Libros";
            this.Load += new System.EventHandler(this.frmConsultarLibro_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblCategorias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAutores)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblLibrosResultado)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView tblLibrosResultado;
        private System.Windows.Forms.Button btnBusqueda;
        private System.Windows.Forms.TextBox txtTerminoBusqueda;
        private System.Windows.Forms.ComboBox cbEditorialesB;
        private System.Windows.Forms.ComboBox cbCategoriasB;
        private System.Windows.Forms.CheckBox checkAutor;
        private System.Windows.Forms.CheckBox checkTitulo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkActivoN;
        private System.Windows.Forms.TextBox txtIsbnN;
        private System.Windows.Forms.TextBox txtNombreLibroN;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView tblCategorias;
        private System.Windows.Forms.ComboBox cbCategoriasN;
        private System.Windows.Forms.DataGridView tblAutores;
        private System.Windows.Forms.ComboBox cbAutoresN;
        private System.Windows.Forms.Button btnAgregarCategoriaN;
        private System.Windows.Forms.Button btnAgregarAutorN;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre_categoria;
        private System.Windows.Forms.DataGridViewButtonColumn controlCat;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre_autor;
        private System.Windows.Forms.DataGridViewButtonColumn control;
        private System.Windows.Forms.Button btnGuardarDatosLibro;
        private System.Windows.Forms.ComboBox cbEditorialesN;
        private System.Windows.Forms.DataGridViewTextBoxColumn cod_libro;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre_libro;
        private System.Windows.Forms.DataGridViewTextBoxColumn editorial;
        private System.Windows.Forms.DataGridViewTextBoxColumn autores;
        private System.Windows.Forms.DataGridViewTextBoxColumn categorias;
        private System.Windows.Forms.Button btnCancelarGuardado;
        private System.Windows.Forms.Button btnActualizarLibro;
        private System.Windows.Forms.Button btnSeleccionarLibro;
    }
}