﻿using Administrador_Biblioteca.Gestor;
using Administrador_Biblioteca.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Administrador_Biblioteca
{
    public partial class frmNuevoPrestamo : Form
    {
         private Modelo.Configuracion CONFIG;
        private GestorPrestamo gestorPrestamo;
        private Prestamo prestamo;

        public frmNuevoPrestamo(Modelo.Configuracion CONFIG, GestorPrestamo gestorPrestamo)
        {
            InitializeComponent();
            this.CONFIG = CONFIG;
            this.gestorPrestamo = gestorPrestamo;
        }

        private void frmNuevoPrestamo_Load(object sender, EventArgs e)
        {
            limpiarFormulario();
            cargarFormularioTemporal();
        }

        private void limpiarFormulario()
        {
            lblDocumentoPersona.Text = "";
            lblNombrePersona.Text = "";
            lblEstadoPersona.Text = "";

            lblCodigoLibroItem.Text = "";
            lblNombreLibroItem.Text = "";
            lblISBNLibroItem.Text = "";
            lblAudorLibroItem.Text = "";
            lblEditorialLibroItem.Text = "";
            txtObservacionLibroItem.Text = "";

            DateTime today = DateTime.Now;
            dtpFechaEsperadaLibroItem.Value = today.AddDays(5);

            tblDetallePrestamoTemporal.Rows.Clear();
            tblDetallePrestamoTemporal.Refresh();
        }

        private void cargarFormularioTemporal()
        {
            try
            {
                prestamo = gestorPrestamo.consultarPrestamoTemporal();
                lblDocumentoPersona.Text = prestamo.persona.tipo_documento + " " + prestamo.persona.documento_persona;
                lblNombrePersona.Text = prestamo.persona.nombres + " " + prestamo.persona.apellidos;
                lblEstadoPersona.Text = "";

                cargarTablaPrestamoTemporal(prestamo.detalle);
            }
            catch (Exception ex)
            {

            }
        }

        private void cargarTablaPrestamoTemporal(List<PrestamoDetalle> listaDetalle)
        {
            tblDetallePrestamoTemporal.Rows.Clear();
            foreach(PrestamoDetalle det in listaDetalle){
                tblDetallePrestamoTemporal.Rows.Add(new object[] { det.cod_libro, det.nombre_libro, det.cantidad, det.fecha_entrega_esperada, det.observacion, "x" });
            }
            tblDetallePrestamoTemporal.Refresh();
        }

        private void btnSeleccionarPersona_Click(object sender, EventArgs e)
        {
            try
            {
                limpiarFormulario();
                CONFIG.llamadoDesdePrestamo = true;
                frmAdministrarLector frm = new frmAdministrarLector(CONFIG, new GestorPersona(gestorPrestamo.conn));
                frm.ShowDialog();
                gestorPrestamo.agregarPersonaPrestamoTemporal(frm.personaSeleccionadoPrestamo, CONFIG.cod_usuario_actual);
                cargarFormularioTemporal();
            }
            catch (Exception ex)
            {

            }
        }

        private void btnBuscarLibro_Click(object sender, EventArgs e)
        {
            try
            {
                limpiarFormulario();
                CONFIG.llamadoDesdePrestamo = true;
                frmAdministrarLibro frm = new frmAdministrarLibro(CONFIG, new GestorLibro(gestorPrestamo.conn));
                frm.ShowDialog();
                llenarItemSeleccionado(frm.libroSeleccionadoPrestamo);
                cargarFormularioTemporal();
            }
            catch (Exception ex)
            {

            }
        }

        private void llenarItemSeleccionado(Libro libro)
        {
            try
            {
                lblCodigoLibroItem.Text = libro.cod_libro;
                lblNombreLibroItem.Text = libro.nombre_libro;
                lblISBNLibroItem.Text = libro.isbn;
                lblAudorLibroItem.Text = String.Join("; ", libro.autores.ToArray());
                lblEditorialLibroItem.Text = libro.editorial;
            }
            catch (Exception ex)
            {

            }
            
        }

        private void btnAgregarAListaTemporal_Click(object sender, EventArgs e)
        {
            try
            {
                PrestamoDetalle detalle = new PrestamoDetalle();
                detalle.cod_libro = lblCodigoLibroItem.Text;
                detalle.fecha_entrega_esperada = dtpFechaEsperadaLibroItem.Value.ToShortDateString();
                detalle.cantidad = Int32.Parse(nudCantidadLibroItem.Value.ToString());
                detalle.observacion = txtObservacionLibroItem.Text;
                gestorPrestamo.agregarLibroPrestamoTemporal(detalle);
                limpiarFormulario();
                cargarFormularioTemporal();
            }
            catch (Exception ex)
            {

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                gestorPrestamo.crearTemporalesPrestamo(); //Elimina y vuelve a crear
                limpiarFormulario();
                cargarFormularioTemporal();
            }
            catch (Exception ex)
            {

            }
        }

        private void tblDetallePrestamoTemporal_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == tblDetallePrestamoTemporal.Columns["eliminar"].Index)
                {
                    string codigoLibro = tblDetallePrestamoTemporal.Rows[e.RowIndex].Cells["cod_libro"].Value.ToString();
                    gestorPrestamo.eliminarLibroPrestamoTemporal(codigoLibro);
                    cargarFormularioTemporal();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void btnGuardarNuevoPrestamo_Click(object sender, EventArgs e)
        {
            try
            {
                bool resultado = gestorPrestamo.guardarPrestamo();
                if (resultado)
                {
                    MessageBox.Show("Prestamo Guardado exitosamente!!");
                    gestorPrestamo.crearTemporalesPrestamo(); //Elimina y vuelve a crear
                    limpiarFormulario();
                    cargarFormularioTemporal();
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
