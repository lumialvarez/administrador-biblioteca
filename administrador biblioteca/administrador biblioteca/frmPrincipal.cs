﻿
using Administrador_Biblioteca.Gestor;
using Administrador_Biblioteca.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Administrador_Biblioteca
{
    public partial class frmPrincipal : Form
    {
        GestorGeneral gestorGeneral;
        GestorLibro gestorLibro;
        GestorPersona gestorPersona;
        GestorPrestamo gestorPrestamo;
        Modelo.Configuracion CONFIG = new Modelo.Configuracion();

        public frmPrincipal(string[] args, GestorGeneral gestor, Modelo.Configuracion CONFIG)
        {
            InitializeComponent();
            this.gestorGeneral = gestor;
            this.CONFIG = CONFIG;
            this.gestorLibro = new GestorLibro(gestor.conn);
            this.gestorPersona = new GestorPersona(gestor.conn);
            this.gestorPrestamo = new GestorPrestamo(gestor.conn);

            inicializarInterfaceUsuario();
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            gestorGeneral.crearTemporalesPrestamo();
            if (!CONFIG.permisos.Contains("SUPER"))
            {
                btn_lectores.Enabled = false;
            }
            gestorGeneral.crearTemporalesPrestamo();
        }

        private void inicializarInterfaceUsuario()
        {
            lblDatosUsuario.Text = CONFIG.usuario_actual + " - " + CONFIG.nombre_persona_usuario_actual;
            if (CONFIG.permisos.Contains("SUPER"))
            {
                lblDatosUsuario.Text += "  (Administrador)";
            }
        }

        private void btn_libros_Click(object sender, EventArgs e)
        {
            frmAdministrarLibro frm = new frmAdministrarLibro(CONFIG, gestorLibro);
            frm.Show();
        }

        private void frmPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btn_lectores_Click(object sender, EventArgs e)
        {
            frmAdministrarLector frm = new frmAdministrarLector(CONFIG, gestorPersona);
            frm.Show();
        }

        private void tbn_usuario_Click(object sender, EventArgs e)
        {
            try
            {
                Modelo.Persona persona = gestorPersona.cargarInfoPersona(CONFIG.cod_usuario_actual);
                frmAdministrarUsuario frm = new frmAdministrarUsuario(CONFIG, gestorPersona, persona);
                frm.ShowDialog();
            }
            catch (Exception ex)
            {

            }
        }

        private void btn_prestamo_Click(object sender, EventArgs e)
        {
            try
            {
                frmNuevoPrestamo frm = new frmNuevoPrestamo(CONFIG, gestorPrestamo);
                frm.Show();
            }
            catch (Exception ex)
            {

            }
        }

        private void btn_devolucion_Click(object sender, EventArgs e)
        {
            try
            {
                frmAdministrarPrestamos frm = new frmAdministrarPrestamos(CONFIG, gestorPrestamo);
                frm.Show();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
