﻿using Administrador_Biblioteca.Modelo;
using PostgresConexion;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_Biblioteca
{
    public class GestorGeneral
    {
        public PgConexion conn;
        private GeneralDAO dao;


        public GestorGeneral(PgConexion conn)
        {
            this.conn = conn;
            dao = new GeneralDAO(conn);
        }

        public GestorGeneral(string usuario, string contraseña, string ip_db, string puerto_db, string nombre_db, bool permiso)
        {
            conn = new PgConexion(usuario, contraseña, ip_db, puerto_db, nombre_db, permiso);
            dao = new GeneralDAO(conn);
        }

        public bool probarConexion()
        {
            return dao.probarConexion();
        }

        public DataTable autenticarUsuario(string nomUsuario, string contraUsuario)
        {
            return dao.autenticarUsuario(nomUsuario, contraUsuario);
        }

        public bool actualizarUsuario(string documentoPersona, string nombreUsuario, String contraseña, bool activo, bool admin)
        {
            return dao.actualizarUsuario(documentoPersona, nombreUsuario, contraseña, activo, admin);
        }

        public bool agregarNuevoUsuario(string documentoPersona, string nombreUsuario, String contraseña, bool activo, bool admin)
        {
            return dao.agregarNuevoUsuario(documentoPersona, nombreUsuario, contraseña, activo, admin);
        }

        public void crearTemporalesPrestamo()
        {
            dao.crearTemporalesPrestamo();
        }

        public Prestamo consultarPrestamoTemporal()
        {
            return dao.consultarPrestamoTemporal();
        }

        public bool agregarPersonaPrestamoTemporal(Persona persona, String documentoResponsable)
        {
            return dao.agregarPersonaPrestamoTemporal(persona, documentoResponsable);
        }

        internal bool agregarLibroPrestamoTemporal(PrestamoDetalle detalle)
        {
            return dao.agregarLibroPrestamoTemporal(detalle);
        }

        internal bool eliminarLibroPrestamoTemporal(string codigoLibro)
        {
            return dao.eliminarLibroPrestamoTemporal(codigoLibro);
        }

        internal bool guardarPrestamo()
        {
            return dao.guardarPrestamo();
        }
    }
}
