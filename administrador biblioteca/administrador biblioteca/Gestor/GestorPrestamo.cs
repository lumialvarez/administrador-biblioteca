﻿using Administrador_Biblioteca.DAO;
using PostgresConexion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_Biblioteca.Gestor
{
    public class GestorPrestamo : GestorGeneral
    {
        private PrestamoDAO dao;
        public GestorPrestamo(PostgresConexion.PgConexion conn) : base (conn)
        {
            this.conn = conn;
            this.dao = new PrestamoDAO(conn);
        }
    }
}
