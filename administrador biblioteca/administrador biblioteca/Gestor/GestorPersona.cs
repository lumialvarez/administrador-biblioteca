﻿using PostgresConexion;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_Biblioteca.Gestor
{
    public class GestorPersona : GestorGeneral
    {
        private PersonaDAO dao;

        public GestorPersona(PgConexion conn) :base (conn)
        {
            this.conn = conn;
            this.dao = new PersonaDAO(conn);
        }

        public DataTable cargarTiposDocumentos()
        {
            return dao.cargarTiposDocumentos();
        }

        internal List<Modelo.Persona> buscarPersonas(bool activos, bool todos, bool usuarios, string filtroDocumento, string filtroNombre, string filtroApellido)
        {
            return dao.buscarPersonas(activos, todos, usuarios, filtroDocumento, filtroNombre, filtroApellido);
        }

        internal Modelo.Persona cargarInfoPersona(string documentoPersona)
        {
            return dao.cargarInfoPersona(documentoPersona);
        }

        internal bool agregarNuevaPersona(Modelo.Persona persona)
        {
            return dao.agregarNuevaPersona(persona);
        }

        internal bool actualizarPersona(Modelo.Persona persona)
        {
            return dao.actualizarPersona(persona);
        }
    }
}
