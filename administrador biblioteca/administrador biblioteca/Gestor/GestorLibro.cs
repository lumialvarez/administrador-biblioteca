﻿using PostgresConexion;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_Biblioteca
{
    public class GestorLibro : GestorGeneral
    {
        private LibroDAO dao;

        public GestorLibro(PostgresConexion.PgConexion conn) :  base (conn)
        {
            this.conn = conn;
            this.dao = new LibroDAO(conn);
        }

        public DataTable cargarCategorias()
        {
            return dao.cargarCategorias();
        }

        public DataTable cargarEditoriales()
        {
            return dao.cargarEditoriales();
        }

        public DataTable cargarAutores(string filtro)
        {
            return dao.cargarAutores(filtro);
        }

        public bool agregarNuevoAutorInsert(string nombreAutor, string apellidoAutor)
        {
            return dao.agregarNuevoAutorInsert(nombreAutor, apellidoAutor);
        }

        internal int agregarNuevoLibro(Modelo.Libro libro)
        {
            return dao.agregarNuevoLibro(libro);
        }

        internal List<Modelo.Libro> buscarLibros(string fitro, string categoria, string editorial, bool nombre, bool autor)
        {
            return dao.buscarLibros(fitro, categoria, editorial, nombre, autor);
        }

        internal Modelo.Libro cargarInfoLibro(int cod_libro)
        {
            return dao.cargarInfoLibro(cod_libro);
        }

        internal int actualizarInfoLibro(Modelo.Libro libro)
        {
            return dao.actualizarInfoLibro(libro);
        }
    }
}
