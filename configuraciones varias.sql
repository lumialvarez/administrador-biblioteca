﻿
CREATE SEQUENCE t_autor_serial START WITH 1;
ALTER TABLE t_autor ALTER COLUMN cod_autor SET DEFAULT nextval('t_autor_serial');


CREATE SEQUENCE t_libro_serial START WITH 1;
ALTER TABLE t_libro ALTER COLUMN cod_libro SET DEFAULT nextval('t_libro_serial');


CREATE SEQUENCE t_editorial_serial START WITH 1;
ALTER TABLE t_editorial ALTER COLUMN cod_editorial SET DEFAULT nextval('t_editorial_serial');


CREATE SEQUENCE t_categoria_serial START WITH 1;
ALTER TABLE t_categoria ALTER COLUMN cod_categoria SET DEFAULT nextval('t_categoria_serial');


CREATE SEQUENCE t_prestamo_serial START WITH 1;
ALTER TABLE t_prestamo ALTER COLUMN cod_prestamo SET DEFAULT nextval('t_prestamo_serial');



----


INSERT INTO t_tipo_documento VALUES (0,'ND','ND');
INSERT INTO t_tipo_documento VALUES (1,'CEDULA DE CIUDADANIA','CC');
INSERT INTO t_tipo_documento VALUES (2,'TARJETA DE IDENTIDAD','TI');
INSERT INTO t_tipo_documento VALUES (3,'REGISTRO CIVIL','RC');
INSERT INTO t_tipo_documento VALUES (4,'CEDULA DE EXTRANJERIA','CE');
INSERT INTO t_tipo_documento VALUES (5,'NUIP','NUIP');
INSERT INTO t_tipo_documento VALUES (6,'NIT','NIT');
INSERT INTO t_tipo_documento VALUES (7,'FN','FN');
INSERT INTO t_tipo_documento VALUES (8,'PASAPORTE','PA');
INSERT INTO t_tipo_documento VALUES (9,'MENOR SIN IDENTIFICACION','MS');
INSERT INTO t_tipo_documento VALUES (10,'ADULTO SIN IDENTIFICACION','AS');

---



ALTER TABLE t_prestamo ALTER COLUMN fecha_prestamo SET DEFAULT current_date;
ALTER TABLE t_prestamo ALTER COLUMN hora_prestamo SET DEFAULT current_time;







